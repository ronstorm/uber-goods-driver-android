package com.data.uber.goods.driver.app.service.network;

import com.data.uber.goods.driver.app.model.driver.DriverLocation;
import com.data.uber.goods.driver.app.model.driver.DriverLocationResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface DriverRestService {
    @PUT("v1/vehicle-location/{driver_id}/")
    Observable<DriverLocationResponse> updateDriverLocation(@Body DriverLocation driverLocation, @Path("driver_id") String driverId);
}

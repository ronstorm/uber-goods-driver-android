package com.data.uber.goods.driver.app.service.network;


import com.data.uber.goods.driver.app.model.GeoLocationResponse;
import com.data.uber.goods.driver.app.model.authentication.UserInfo;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RegistrationService {
    @GET("/maps/api/geocode/json")
    Observable<GeoLocationResponse> getAddressFromLatLng(@Query("latlng") String latlng, @Query("key") String key);

    @POST("v1/driver/")
    Observable<UserInfo> registerUser(@Body UserInfo userInfo);
}

package com.data.uber.goods.driver.app.service.network;


import com.data.uber.goods.driver.app.model.settings.AddVehicle;
import com.data.uber.goods.driver.app.model.settings.CarType;
import com.data.uber.goods.driver.app.model.settings.UpdateVehicle;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface VehicleRestService
{
    @GET("v1/vehicle/{id}/")
    Observable<AddVehicle> getVehicleList(@Path("id") String driverId);

    @GET("v1/transportation-category/")
    Observable<List<CarType>> getCarType();

    @POST("v1/vehicle/")
    Observable<AddVehicle> addVehicle(@Body AddVehicle vehicle);

    @POST("v1/vehicle/")
    Observable<AddVehicle> updateVehicle(@Body UpdateVehicle vehicle);
}

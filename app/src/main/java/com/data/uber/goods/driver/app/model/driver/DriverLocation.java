package com.data.uber.goods.driver.app.model.driver;

import com.data.uber.goods.driver.app.model.mytrips.Location;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverLocation {
    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("trip_id")
    @Expose
    private Integer tripId;

    @SerializedName("rotation")
    @Expose
    private float rotation;

    @SerializedName("bearing")
    @Expose
    private float bearing;


    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getTripId() {
        return tripId;
    }

    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public float getBearing() {
        return bearing;
    }

    public void setBearing(float bearing) {
        this.bearing = bearing;
    }
}

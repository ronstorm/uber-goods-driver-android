package com.data.uber.goods.driver.app.view;

import com.data.uber.goods.driver.app.model.authentication.CountryEntity;

import java.util.List;


public interface SelectCountryInterface {
    void setCountryListOnView(List<CountryEntity> countryEntity);
}

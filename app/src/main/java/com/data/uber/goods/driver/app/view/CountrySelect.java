package com.data.uber.goods.driver.app.view;

public interface CountrySelect {
    void selectCountry(int id, String countryName);
}

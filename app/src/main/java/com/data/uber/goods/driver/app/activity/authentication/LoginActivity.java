package com.data.uber.goods.driver.app.activity.authentication;

import android.Manifest;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.activity.BaseUIController;
import com.data.uber.goods.driver.app.activity.dashboard.DashBoardActivity;
import com.data.uber.goods.driver.app.model.authentication.LoginCredentials;
import com.data.uber.goods.driver.app.model.authentication.UserInfo;
import com.data.uber.goods.driver.app.model.authentication.UserLoginDetail;
import com.data.uber.goods.driver.app.presenter.authentication.LoginPresenter;
import com.data.uber.goods.driver.app.util.GmapView;
import com.data.uber.goods.driver.app.util.firebase.FireBaseInstanceIdService;
import com.data.uber.goods.driver.app.view.authentication.LoginIterface;
import com.google.android.gms.maps.MapView;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class LoginActivity extends BaseUIController implements View.OnClickListener, LoginIterface, Validator.ValidationListener{

    @NotEmpty
    @Email
    @InjectView(R.id.editTextEmailAddress)
    EditText editTextEmailAddress;

    @NotEmpty
    @InjectView(R.id.editTextPassword)
    EditText editTextPassword;

    @InjectView(R.id.forgotPasswordBtn)
    Button forgetPassword;

    private Button createAccountBtn;
    private Button loginBtn;
    private LoginPresenter loginPresenter;
    private Validator validator;
    GmapView gmapView;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.setLayoutType(AppEngine.getInstance().constants.EMPTY_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.login_layout_driver);

        loginPresenter = new LoginPresenter(this, this);

        ButterKnife.inject(this);
        validator = new Validator(this);
        validator.setValidationListener(this);

        this.init(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEngine.getInstance().googleMapView.refreshLocManager(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //AppEngine.getInstance().googleMapView.detachMap();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //AppEngine.getInstance().googleMapView.detachMap();
    }

    private void init(Bundle savedInstanceState) {
        bundle = getIntent().getExtras();
        LinearLayout mainOverlay = this.findViewById(R.id.mainOverlay);
        mainOverlay.setOnClickListener(this);
        this.createAccountBtn = this.findViewById(R.id.createAccountBtn);
        this.createAccountBtn.setOnClickListener(this);
        this.loginBtn = this.findViewById(R.id.loginBtn);
        this.loginBtn.setOnClickListener(this);
        gmapView = new GmapView();

        gmapView.setMapCameraOffset(true);
        gmapView.setLoadCustomMarker(false);
        gmapView.initView(this, (MapView) findViewById(R.id.mapView), savedInstanceState, true);
        gmapView.getMapView().onStart();
        AppEngine.getInstance().googleMapView.setGoogleMapLocationListner(this.gmapView);
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        AppEngine.getInstance().googleMapView.requestPermissionCallback(requestCode, grantResults, this);
        if(!AppEngine.getInstance().sharedPrefUtils.getPref(AppEngine.getInstance().constants.OAUTH_ACCESS_TOKEN, this).isEmpty()){
            showloading();
            loginPresenter.getDriverProfile();
        }
    }

    @Override
    public void onClick(View view) {
        if(view.equals(this.createAccountBtn)) {
            AppEngine.getInstance().androidIntent.startActivity(this, RegistrationActivity.class);
        }
        if(view.equals(this.loginBtn)){
            showloading();
            validator.validate();
        }
    }

    @Override
    public void loginAPI() {
        AppEngine.getInstance().constants.loggenIn = true;
        FireBaseInstanceIdService fireBaseInstanceIdService = new FireBaseInstanceIdService();
        fireBaseInstanceIdService.registerFcmToken(this);
        hideloading();
        OpenIntent();
    }

    @Override
    public void userProfile(UserInfo userInfo) {
        hideloading();
        OpenIntent();
    }

    @Override
    public void error() {
        hideloading();
    }

    @Override
    public void onValidationSucceeded() {
        LoginCredentials loginCredentials = new LoginCredentials();
        loginCredentials.setUsername(this.editTextEmailAddress.getText().toString());
        loginCredentials.setPassword(this.editTextPassword.getText().toString());
        loginPresenter.driverLogin(this.editTextEmailAddress.getText().toString(), this.editTextPassword.getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
        hideloading();
    }

    @OnClick(R.id.forgotPasswordBtn)
    public void forgetPasswordClick() {
        AppEngine.getInstance().androidIntent.startActivity(this, ForgetPasswordMainActivity.class);
    }

    private void OpenIntent(){
        if(bundle!=null){
            for (String key: bundle.keySet())
            {
                Log.d ("myApplication", key + " is a key in the bundle: " + bundle.get(key));
            }
            Intent intent = new Intent(this, DashBoardActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }else{
            AppEngine.getInstance().androidIntent.startActivity(this, DashBoardActivity.class);
        }
    }

}

package com.data.uber.goods.driver.app.presenter.report;

import android.content.Context;
import android.widget.Toast;


import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.model.report.IssueCreateRequest;
import com.data.uber.goods.driver.app.model.report.IssueCreateResponse;
import com.data.uber.goods.driver.app.service.network.IssueApiService;
import com.data.uber.goods.driver.app.view.report.ReportIssueSubmitInterface;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


public class ReportSubmitPresenter {
    private Context context;
    private ReportIssueSubmitInterface reportIssueSubmitInterface;

    public ReportSubmitPresenter(Context context, ReportIssueSubmitInterface reportIssueSubmitInterface) {
        this.context = context;
        this.reportIssueSubmitInterface = reportIssueSubmitInterface;
    }

    public void submitIssue(IssueCreateRequest issueCreateRequest) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(IssueApiService.class, context).submitIssue(issueCreateRequest),
                new Observer<IssueCreateResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(IssueCreateResponse issueCreateResponse) {
                        Toast.makeText(context, "Issue Submitted Successfully", Toast.LENGTH_LONG).show();
                        reportIssueSubmitInterface.onSubmitApiSuccess(issueCreateResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                        reportIssueSubmitInterface.onSubmitApiError();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}

package com.data.uber.goods.driver.app.notification;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.content.Intent;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.activity.authentication.LoginActivity;
import com.data.uber.goods.driver.app.activity.dashboard.DashBoardActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.util.Map;

public class DriverFoundNotification extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private static int count = 0;

    @Override
    public void onMessageReceived(RemoteMessage message) {
        Log.d(TAG,"NOTI is CUMING");

        Gson gson = new Gson();
        Log.d("notificationPayload", gson.toJson(message.getData()));

        AppEngine.getInstance().sharedPrefUtils.putPref("tripId", message.getData().get("tripId"), this);

        if(Integer.parseInt(message.getData().get("id")) == 0){
            Log.d("Source", message.getData().get("pickUpStr"));
            Log.d("Destination", message.getData().get("destinationStr"));
            Log.d("customerId", message.getData().get("customerId"));

            AppEngine.getInstance().sharedPrefUtils.putPref("pickUp", message.getData().get("pickUp"), this);
            AppEngine.getInstance().sharedPrefUtils.putPref("destination", message.getData().get("destination"), this);

            AppEngine.getInstance().sharedPrefUtils.putPref("pickUpStr", message.getData().get("pickUpStr"), this);
            AppEngine.getInstance().sharedPrefUtils.putPref("destinationStr", message.getData().get("destinationStr"), this);
            AppEngine.getInstance().sharedPrefUtils.putPref("customerId", message.getData().get("customerId"), this);

            LocalBroadcastManager broadcaster = LocalBroadcastManager.getInstance(getBaseContext());
            Intent refreshIntent = new Intent("NotificationReceived");
            refreshIntent.putExtra("tripState", 0);
            AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "acceptRequest", this);
            broadcaster.sendBroadcast(refreshIntent);
        }
        if(Integer.parseInt(message.getData().get("id")) == 7){
            AppEngine.getInstance().sharedPrefUtils.putPref("amount", message.getData().get("amount"), this);
            AppEngine.getInstance().sharedPrefUtils.putPref("medium", message.getData().get("medium"), this);
            AppEngine.getInstance().sharedPrefUtils.putPref("customerId", message.getData().get("customerId"), this);
            LocalBroadcastManager broadcaster = LocalBroadcastManager.getInstance(getBaseContext());
            Intent refreshIntent = new Intent("NotificationReceived");
            refreshIntent.putExtra("tripState", 7);
            AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "paymentRcv", this);
            broadcaster.sendBroadcast(refreshIntent);
        }

        sendMyNotification(message.getNotification().getBody(), message.getData());

    }


    private void sendMyNotification(String message, Map<String, String> data) {

        //On click of notification it redirect to this Activity
        Intent intent = new Intent(this, DashBoardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bundle bundle = new Bundle();
//        bundle.putString("openDialog", "open");
//        bundle.putInt("tripState", Integer.parseInt(data.get("id")));
//        bundle.putInt("tripId", Integer.parseInt(data.get("tripId")));
        intent.putExtras(bundle);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel(notificationManager);
        }

        Uri soundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "Uber Goods")
                .setSmallIcon(R.mipmap.logo)
                .setContentTitle("Customer Found")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);



        notificationManager.notify(0, notificationBuilder.build());
    }

    @TargetApi(26)
    private void createChannel(NotificationManager notificationManager) {
        String name = "Uber Goods";
        String description = "Notification";
        int importance = NotificationManager.IMPORTANCE_DEFAULT;

        NotificationChannel mChannel = new NotificationChannel(name, name, importance);
        mChannel.setDescription(description);
        mChannel.enableLights(true);
        mChannel.setLightColor(Color.BLUE);
        notificationManager.createNotificationChannel(mChannel);
    }
}

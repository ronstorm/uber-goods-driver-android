package com.data.uber.goods.driver.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GeoLocationResponse {
    @SerializedName("results")
    @Expose
    private List<GmapAPIGeoLocation> results;
    @SerializedName("status")
    @Expose
    private String status;

    public List<GmapAPIGeoLocation> getResults() {
        return results;
    }

    public void setResults(List<GmapAPIGeoLocation> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

package com.data.uber.goods.driver.app.model.rating;

public class Rating {
    private int trip;
    private int rating;
    private String comment;

    public int getTrip() {
        return trip;
    }
    public void setTrip(int trip) {
        this.trip = trip;
    }
    public int getRating() {
        return rating;
    }
    public void setRating(int rating) {
        this.rating = rating;
    }
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
}

package com.data.uber.goods.driver.app.util.socket.io;

import org.json.JSONObject;

interface SocketIOListner {
    public void onMessageReceive(JSONObject data);
    public void onImageMessageReceive(JSONObject data);
}

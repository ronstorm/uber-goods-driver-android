package com.data.uber.goods.driver.app.model.mytrips;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 6/13/2018.
 */

public class AcceptTrip{
    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("location")
    @Expose
    private String location;

    public AcceptTrip(){

    }

    public AcceptTrip(boolean status){
        this.status = status;
    }
    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}

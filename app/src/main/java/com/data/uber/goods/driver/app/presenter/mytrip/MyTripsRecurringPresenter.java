package com.data.uber.goods.driver.app.presenter.mytrip;

import android.content.Context;
import android.widget.Toast;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.model.mytrips.DriverProfile;
import com.data.uber.goods.driver.app.model.mytrips.TripItem;
import com.data.uber.goods.driver.app.service.network.TripRestService;
import com.data.uber.goods.driver.app.view.mytrip.MytripsRecurringInterface;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

/**
 * Created by kud-wtag on 6/24/18.
 */

public class MyTripsRecurringPresenter {
    private Context context;
    private MytripsRecurringInterface mytripsRecurringInterface;
    private List<TripItem> tripItemListPrivate;

    public MyTripsRecurringPresenter(Context context, MytripsRecurringInterface mytripsRecurringInterface) {
        this.context = context;
        this.mytripsRecurringInterface = mytripsRecurringInterface;
    }

    public void getAllTrips() {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(TripRestService.class, context).getAllRecurringTrips(),
                new Observer<List<TripItem>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull List<TripItem> tripItemList) {

                        tripItemListPrivate = tripItemList;
                        for(int i=0; i<tripItemList.size(); i++) {
                            getDriverProfile(tripItemList.get(i).getDriver(), tripItemListPrivate.get(i));
                        }
                        mytripsRecurringInterface.tripsFetchSuccess(tripItemListPrivate);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        mytripsRecurringInterface.tripsFetchError();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getDriverProfile(String driverId, final TripItem tripItem) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(TripRestService.class, context).
                        getCustomerProfileInfo(driverId),
                new Observer<DriverProfile>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                        //Toast.makeText(context, "Error " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(@NonNull DriverProfile userInfo) {
                        tripItem.setDriverProfile(userInfo);
                        mytripsRecurringInterface.tripsFetchSuccess(tripItemListPrivate);
                    }
                });
    }
}

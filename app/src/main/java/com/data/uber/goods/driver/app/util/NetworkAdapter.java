package com.data.uber.goods.driver.app.util;


import android.content.Context;

import com.data.uber.goods.driver.app.AppEngine;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class NetworkAdapter {

    OkHttpClient okClient;
    TokenAuthenticator tokenAuthenticator;

    public NetworkAdapter() {
        tokenAuthenticator = new TokenAuthenticator();
        okClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request().newBuilder().addHeader("Authorization", "").build();
                        return chain.proceed(request);
                    }
                })
                .addInterceptor(tokenAuthenticator)
                .build();
    }
    public <T> T create(Class<T> service) {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(AppEngine.getInstance().constants.API_BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(service);
    }

    public <T> T callAPI(Class<T> service, final Context context) {
        System.out.print("AUTHORIZATION::: "+AppEngine.getInstance().sharedPrefUtils.getPref(AppEngine.getInstance().constants.OAUTH_TOKEN_TYPE, context)+" "+AppEngine.getInstance().sharedPrefUtils.getPref(AppEngine.getInstance().constants.OAUTH_ACCESS_TOKEN, context));
        tokenAuthenticator.setContext(context);
        okClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request().newBuilder().addHeader("Authorization", AppEngine.getInstance().sharedPrefUtils.getPref(AppEngine.getInstance().constants.OAUTH_TOKEN_TYPE, context)+" "+AppEngine.getInstance().sharedPrefUtils.getPref(AppEngine.getInstance().constants.OAUTH_ACCESS_TOKEN, context)).build();
                        return chain.proceed(request);
                    }
                })
                .addInterceptor(tokenAuthenticator)
                .build();
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(AppEngine.getInstance().constants.API_BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okClient)
                .build();
        return retrofit.create(service);
    }
    public <T> T oauthLogin(Class<T> service) {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(AppEngine.getInstance().constants.API_BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(service);
    }
    public <T> T create(Class<T> service, String BASE_URL) {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(service);
    }
    public <T> void subscriber(Observable<T> observable, Observer<T> observer) {
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }
}

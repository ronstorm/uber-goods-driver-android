package com.data.uber.goods.driver.app.model.settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateVehicle {
    @SerializedName("model_name")
    @Expose
    private String modelName;
    @SerializedName("car_type")
    @Expose
    private int carType;
    @SerializedName("registration_number")
    @Expose
    private String registrationNumber;
    @SerializedName("chassis_number")
    @Expose
    private String chassisNumber;
    @SerializedName("insurance_number")
    @Expose
    private String insuranceNumber;
    @SerializedName("max_capacity")
    @Expose
    private int maxCapacity;
    @SerializedName("allowed_capacity")
    @Expose
    private int allowedCapacity;

    public UpdateVehicle(){

    }

    public UpdateVehicle(String modelName, Integer carType, String registrationNumber, String chassisNumber, String insuranceNumber, Integer maxCapacity, Integer allowedCapacity) {
        this.modelName = modelName;
        this.carType = carType;
        this.registrationNumber = registrationNumber;
        this.chassisNumber = chassisNumber;
        this.insuranceNumber = insuranceNumber;
        this.maxCapacity = maxCapacity;
        this.allowedCapacity = allowedCapacity;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public int getCarType() {
        return carType;
    }

    public void setCarType(int carType) {
        this.carType = carType;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public int getAllowedCapacity() {
        return allowedCapacity;
    }

    public void setAllowedCapacity(int allowedCapacity) {
        this.allowedCapacity = allowedCapacity;
    }

}



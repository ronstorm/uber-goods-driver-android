package com.data.uber.goods.driver.app.activity.mytrips;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.model.mytrips.TripItem;
import com.data.uber.goods.driver.app.presenter.mytrip.MyTripsRecurringPresenter;
import com.data.uber.goods.driver.app.view.mytrip.MytripsRecurringInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kud-wtag on 6/24/18.
 */

public class RecurringTripFrament extends Fragment implements MytripsRecurringInterface {
    ListView myTripListView;
    List<TripItem> myTripList;
    MyTripRecurringListAdapter myTripRecurringListAdapter;
    MyTripsRecurringPresenter myTripsRecurringPresenter;
    Context context;

    public void initialize(Context context) {
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_my_trip_recurring, container, false);
        init(savedInstanceState, view);
        return view;
    }

    private void init(Bundle savedInstanceState, View view) {
        myTripListView = view.findViewById(R.id.tripsListViewRecurring);
        myTripList = new ArrayList<>();
        myTripRecurringListAdapter = new MyTripRecurringListAdapter(context, myTripList);
        myTripListView.setAdapter(myTripRecurringListAdapter);

        myTripsRecurringPresenter = new MyTripsRecurringPresenter(context, this);
        myTripsRecurringPresenter.getAllTrips();
    }

    @Override
    public void tripsFetchSuccess(List<TripItem> tripItemList) {
        this.myTripList.clear();
        this.myTripList.addAll(tripItemList);
        myTripRecurringListAdapter.notifyDataSetChanged();
    }

    @Override
    public void tripsFetchError() {

    }
}

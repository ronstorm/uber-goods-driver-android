package com.data.uber.goods.driver.app.view.authentication;

import com.data.uber.goods.driver.app.model.authentication.UserInfo;
import com.data.uber.goods.driver.app.model.authentication.UserLoginDetail;

public interface LoginIterface {
    void loginAPI();
    void userProfile(UserInfo userInfo);
    void error();
}

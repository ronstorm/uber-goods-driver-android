package com.data.uber.goods.driver.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GmapAPIGeoLocation {
    @SerializedName("formatted_address")
    @Expose
    private String formatted_address;

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }
}

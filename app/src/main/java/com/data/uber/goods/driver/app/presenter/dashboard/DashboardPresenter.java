package com.data.uber.goods.driver.app.presenter.dashboard;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.BuildConfig;
import com.data.uber.goods.driver.app.model.authentication.CountryEntity;
import com.data.uber.goods.driver.app.model.authentication.UserInfo;
import com.data.uber.goods.driver.app.model.driver.DriverLocation;
import com.data.uber.goods.driver.app.model.driver.DriverLocationResponse;
import com.data.uber.goods.driver.app.model.mytrips.AcceptTrip;
import com.data.uber.goods.driver.app.model.mytrips.AcceptTripResponse;
import com.data.uber.goods.driver.app.model.mytrips.CustomerProfile;
import com.data.uber.goods.driver.app.model.mytrips.DriverProfile;
import com.data.uber.goods.driver.app.model.mytrips.Location;
import com.data.uber.goods.driver.app.model.mytrips.TripItem;
import com.data.uber.goods.driver.app.model.rating.Rating;
import com.data.uber.goods.driver.app.presenter.authentication.EditProfilePagesActivity;
import com.data.uber.goods.driver.app.service.network.CountryListRestService;
import com.data.uber.goods.driver.app.service.network.DashboardRestService;
import com.data.uber.goods.driver.app.service.network.DriverRestService;
import com.data.uber.goods.driver.app.service.network.RatingRestService;
import com.data.uber.goods.driver.app.service.network.TripRestService;
import com.data.uber.goods.driver.app.util.DirectionsJSONParser;
import com.data.uber.goods.driver.app.view.SelectCountryInterface;
import com.data.uber.goods.driver.app.view.dashboard.DashboardInterface;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


public class DashboardPresenter {
    private final Context context;
    private DashboardInterface dashboardInterface;

    public DashboardPresenter(Context context, DashboardInterface dashboardInterface) {
        this.context = context;
        this.dashboardInterface = dashboardInterface;
    }

    public void getDriverProfile() {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(DashboardRestService.class, context).
                        getDriverProfileInfo(),
            new Observer<UserInfo>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {

                }
                @Override
                public void onError(@NonNull Throwable e) {
                    Toast.makeText(context, "Dashboard" + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                @Override
                public void onComplete() {

                }
                @Override
                public void onNext(@NonNull UserInfo userInfo) {
                    AppEngine.getInstance().sharedPrefUtils.putPref("driverId", String.valueOf(userInfo.getId()), context);
                    dashboardInterface.setUserDetails(userInfo);
                }
        });
    }

    public void getCustomerProfile(String driverId) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(DashboardRestService.class, context).
                        getCustomerProfileInfo(driverId),
                new Observer<CustomerProfile>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        Toast.makeText(context, "Error " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull CustomerProfile customerProfile) {
                        dashboardInterface.getCustomerProfile(customerProfile);
                    }
                });
    }

    public void acceptRequest(AcceptTrip acceptTrip) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(DashboardRestService.class, context).
                        acceptRequest(acceptTrip, AppEngine.getInstance().sharedPrefUtils.getPref("tripId", context)),
                new Observer<AcceptTripResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(context, "Accept Request" + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(@NonNull AcceptTripResponse acceptTripResponse) {
                        dashboardInterface.acceptRequest();
                    }
                });
    }

    public void onTheWay(AcceptTrip acceptTrip) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(DashboardRestService.class, context).
                        onTheWay(acceptTrip, AppEngine.getInstance().sharedPrefUtils.getPref("tripId", context)),
                new Observer<AcceptTripResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(context, "OnTheWay Request" + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(@NonNull AcceptTripResponse acceptTripResponse) {
                        dashboardInterface.onTheWay();
                    }
                });
    }

    public void startTrip(AcceptTrip acceptTrip) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(DashboardRestService.class, context).
                        startTrip(acceptTrip, AppEngine.getInstance().sharedPrefUtils.getPref("tripId", context)),
                new Observer<AcceptTripResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(context, "OnTheWay Request" + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(@NonNull AcceptTripResponse acceptTripResponse) {
                        dashboardInterface.startTrip();
                    }
                });
    }

    public void endTrip(AcceptTrip acceptTrip) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(DashboardRestService.class, context).
                        endTrip(acceptTrip, AppEngine.getInstance().sharedPrefUtils.getPref("tripId", context)),
                new Observer<AcceptTripResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(context, "OnTheWay Request" + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(@NonNull AcceptTripResponse acceptTripResponse) {
                        dashboardInterface.endTrip();
                    }
                });
    }

    public void rejectTrip() {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(DashboardRestService.class, context).
                        rejectTrip(AppEngine.getInstance().sharedPrefUtils.getPref("tripId", context)),
                new Observer<AcceptTripResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(context, "reject Request" + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(@NonNull AcceptTripResponse acceptTripResponse) {
                        dashboardInterface.rejectTripAnyState();
                    }
                });
    }

    public void updateDriverLocation(android.location.Location locationDriver, float rotation) {
        Location location = new Location();
        location.setLatitude(locationDriver.getLatitude());
        location.setLongitude(locationDriver.getLongitude());
        DriverLocation driverLocation = new DriverLocation();
        Gson gson = new Gson();
        driverLocation.setLocation(gson.toJson(location));
        driverLocation.setTripId(Integer.parseInt(AppEngine.getInstance().sharedPrefUtils.getPref("tripId", context)));
        driverLocation.setBearing(locationDriver.getBearing());
        driverLocation.setRotation(rotation);
        Log.d("driverlocation", gson.toJson(driverLocation));
        Log.d("driverlocationrotation", String.valueOf(rotation));
        Log.d("driverlocationbearing", String.valueOf(locationDriver.getBearing()));
        Log.d("driverlocation", AppEngine.getInstance().sharedPrefUtils.getPref("driverId", context));
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(DriverRestService.class, context).updateDriverLocation(
                driverLocation, AppEngine.getInstance().sharedPrefUtils.getPref("driverId", context)),
                new Observer<DriverLocationResponse>() {
                    @Override
                    public void onSubscribe(Disposable disposable) {

                    }

                    @Override
                    public void onNext(DriverLocationResponse driverLocationResponse) {
                        Toast.makeText(context, "Driver Location Updated", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public void getDirection(String d) {
        String s = String.valueOf(AppEngine.getInstance().constants.LOCATION.getLatitude())+","+String.valueOf(AppEngine.getInstance().constants.LOCATION.getLongitude());
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.create(DashboardRestService.class, "https://maps.googleapis.com")
                .getAddressFromLatLng(s, d, BuildConfig.GoogleApiKey), new Observer<String>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                Toast.makeText(context, "Logging in....", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onError(@NonNull Throwable e) {
                Toast.makeText(context, "Error Occured", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onComplete() {

            }
            @Override
            public void onNext(@NonNull String data) {
                //https://stackoverflow.com/questions/14710744/how-to-draw-road-directions-between-two-geocodes-in-android-google-map-v2
                List<List<HashMap<String, String>>> routes = null;

                try{
                    DirectionsJSONParser parser = new DirectionsJSONParser();

                    // Starts parsing data
                    JSONObject jsonObject = new JSONObject(data);
                    routes = parser.parse(jsonObject);
                    System.out.println(routes.size());
                }catch(Exception e){
                    e.printStackTrace();
                }
                dashboardInterface.setDirection(routes);

            }
        });
    }

    public void getTripDirection(String s, String d) {
        //String s = String.valueOf(AppEngine.getInstance().constants.LOCATION.getLatitude())+","+String.valueOf(AppEngine.getInstance().constants.LOCATION.getLongitude());
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.create(DashboardRestService.class, "https://maps.googleapis.com")
                .getAddressFromLatLng(s, d, BuildConfig.GoogleApiKey), new Observer<String>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                //Toast.makeText(context, "Logging in....", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onError(@NonNull Throwable e) {
                Toast.makeText(context, "Error Occured", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onComplete() {

            }
            @Override
            public void onNext(@NonNull String data) {
                //https://stackoverflow.com/questions/14710744/how-to-draw-road-directions-between-two-geocodes-in-android-google-map-v2
                List<List<HashMap<String, String>>> routes = null;

                try{
                    //JSONArray jo=jsonObject.getJSONArray("geocoded_waypoints");
                    //System.out.println(jo.getJSONObject(0).getString("geocoder_status"));
                    DirectionsJSONParser parser = new DirectionsJSONParser();

                    // Starts parsing data
                    JSONObject jsonObject = new JSONObject(data);
                    routes = parser.parse(jsonObject);
                    System.out.println(routes.size());
                }catch(Exception e){
                    e.printStackTrace();
                }
                dashboardInterface.setDirection(routes);

            }
        });
    }

    public void submitRating(Rating rating) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(RatingRestService.class, context).submitRating(rating),
                new Observer<AcceptTripResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(@NonNull AcceptTripResponse acceptTripResponse) {
                       dashboardInterface.setRating();
                    }
                });
    }

}

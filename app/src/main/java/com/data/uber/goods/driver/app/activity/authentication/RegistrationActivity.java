package com.data.uber.goods.driver.app.activity.authentication;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.databinding.RegistrationLayoutDriverBinding;
import com.data.uber.goods.driver.app.model.authentication.CountryEntity;
import com.data.uber.goods.driver.app.model.authentication.User;
import com.data.uber.goods.driver.app.model.authentication.UserInfo;
import com.data.uber.goods.driver.app.presenter.authentication.EditProfilePagesActivity;
import com.data.uber.goods.driver.app.presenter.authentication.RegistrationPresenter;
import com.data.uber.goods.driver.app.presenter.authentication.SelectCountry;
import com.data.uber.goods.driver.app.util.DatePickerClass;
import com.data.uber.goods.driver.app.util.GoogleMapLocationListner;
import com.data.uber.goods.driver.app.view.CountrySelect;
import com.data.uber.goods.driver.app.view.SelectCountryCallBack;
import com.data.uber.goods.driver.app.view.SelectCountryInterface;
import com.data.uber.goods.driver.app.view.authentication.RegistrationInterface;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmEmail;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class RegistrationActivity extends AppCompatActivity implements SelectCountryCallBack,RegistrationInterface, GoogleMapLocationListner, Validator.ValidationListener {

    @InjectView(R.id.uploadOptionHolder)
    RelativeLayout uploadOptionHolder;

    @InjectView(R.id.uploadOption)
    LinearLayout uploadOption;

    @InjectView(R.id.addProfilePucture)
    ImageView addProfilePucture;

    @InjectView(R.id.showProfilePicture)
    ImageView showProfilePicture;

    @InjectView(R.id.cameraBtn)
    LinearLayout cameraBtn;

    @InjectView(R.id.imageBtn)
    LinearLayout imageBtn;

    @InjectView(R.id.TOC)
    RadioButton toc;

    @InjectView(R.id.goBackBtn)
    Button goBackBtn;

    @InjectView(R.id.signInBtn)
    Button signInBtn;

    @InjectView(R.id.radioProfileType)
    RadioGroup profileType;

    @NotEmpty
    @InjectView(R.id.birthDate)
    EditText birthDate;

    @NotEmpty
    @InjectView(R.id.licenseRenewalDate)
    EditText licenseRenewalDate;

    @NotEmpty
    @InjectView(R.id.passportExpiryDate)
    EditText passportExpiryDate;

    @InjectView(R.id.gender)
    Spinner genderSpinner;

    @NotEmpty
    @InjectView(R.id.nationality)
    EditText nationality;

    @NotEmpty
    @InjectView(R.id.name)
    EditText name;

    @NotEmpty
    @Email
    @InjectView(R.id.email)
    EditText email;

    @NotEmpty
    @InjectView(R.id.password)
    EditText password;

    @NotEmpty
    @InjectView(R.id.confirmPassword)
    EditText confirmPassword;

    @NotEmpty
    @InjectView(R.id.mobileNumber)
    EditText mobileNumber;

    @NotEmpty
    @InjectView(R.id.licenseNumber)
    EditText licenseNumber;

    @NotEmpty
    @InjectView(R.id.passportNumber)
    EditText passportNumber;

    @NotEmpty
    @InjectView(R.id.workPermitNumber)
    EditText workPermitNumber;

    @NotEmpty
    @InjectView(R.id.areaOfInterest)
    EditText areaOfInterest;

    @NotEmpty
    @InjectView(R.id.defaultLocation)
    EditText defaultLocation;

    private RegistrationPresenter registrationPresenter;
    private RegistrationLayoutDriverBinding registrationLayoutDriverBinding;
    private UserInfo userInfo;
    private RadioButton radioProfileType;
    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registrationLayoutDriverBinding = DataBindingUtil.setContentView(this, R.layout.registration_layout_driver);

        userInfo = new UserInfo(new User("","",""), "","","",1,"","","","","","","","","");
        registrationLayoutDriverBinding.setUserInfo(userInfo);
        ButterKnife.inject(this);

        validator = new Validator(this);
        validator.setValidationListener(this);

        AppEngine.getInstance().googleMapView.setGoogleMapLocationListner(this);
        registrationPresenter = new RegistrationPresenter(this, this);
        if(!AppEngine.getInstance().constants.CURRENT_LOCATION.equals(""))this.registrationPresenter.getDefaultLocation(AppEngine.getInstance().constants.CURRENT_LOCATION);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.gender, R.layout.spinner_layout);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(adapter);
    }

    public void setData(){
        int selectedId=profileType.getCheckedRadioButtonId();
        radioProfileType=(RadioButton)findViewById(selectedId);
        if(radioProfileType.getText().equals("Individual")){
            this.userInfo.setProfileType("I");
        }else{
            this.userInfo.setProfileType("C");
        }

        if(this.genderSpinner.getSelectedItem().equals("Male")){
            this.userInfo.setGender("M");
        }else{
            this.userInfo.setGender("F");
        }
    }

    @OnClick(R.id.nationality)
    public void setNationality(){
        Intent intent = new Intent(this, EditProfilePagesActivity.class);
        startActivityForResult(intent, AppEngine.getInstance().constants.RESULT_CODE_SELECT_COUNTRY);
    }

    @OnClick(R.id.addProfilePucture)
    public void addProfilePictureClick() {
        if(!AppEngine.getInstance().animation.isFading()) {
            AppEngine.getInstance().animation.slideUp(uploadOption, 500);
            AppEngine.getInstance().animation.setInitFadeing(true);
            AppEngine.getInstance().animation.fadeIn(this, uploadOptionHolder);
        }
    }

    @OnClick(R.id.birthDate)
    public void setBirthDate(){
        DatePickerClass datePickerClass = new DatePickerClass(this, R.id.birthDate);
        this.birthDate.setError(null);
    }

    @OnClick(R.id.licenseRenewalDate)
    public void setLicenseRenewalDate(){
        DatePickerClass datePickerClass = new DatePickerClass(this, R.id.licenseRenewalDate);
        this.licenseRenewalDate.setError(null);
    }

    @OnClick(R.id.passportExpiryDate)
    public void setPassportExpiryDate(){
        DatePickerClass datePickerClass = new DatePickerClass(this, R.id.passportExpiryDate);
        this.passportExpiryDate.setError(null);
    }

    @OnClick(R.id.cameraBtn)
    public void cameraBtnClick() {
        this.requestPermission();
    }

    @OnClick(R.id.imageBtn)
    public void imageBtnClick() {
        AppEngine.getInstance().androidIntent.startFileSelectActivity(this, AppEngine.getInstance().constants.MESSAGE_ACTIVITY_RESULT_CODE_FILE);
    }

    @OnClick(R.id.TOC)
    public void tocClick() {
        this.toc.toggle();
    }

    @OnClick(R.id.goBackBtn)
    public void goBackBtnClick() {
        this.finish();
    }

    @OnClick(R.id.signInBtn)
    public void setSignInBtnClick() {
        this.finish();
    }

    @OnClick(R.id.signUpBtn)
    public void registerUser(){
        validator.validate();
    }

    @Override
    public void setDefaultLocation(String location) {
        this.defaultLocation.setText(location);
    }

    @Override
    public void onLocationChanged(Location location) {
        this.registrationPresenter.getDefaultLocation(String.valueOf(location.getLatitude())+','+String.valueOf(location.getLongitude()));
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 875);
        } else {
            AppEngine.getInstance().androidIntent.startCameraActivity(this, AppEngine.getInstance().constants.MESSAGE_ACTIVITY_RESULT_CODE_CAMERA);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 875 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            AppEngine.getInstance().androidIntent.startCameraActivity(this, AppEngine.getInstance().constants.MESSAGE_ACTIVITY_RESULT_CODE_CAMERA);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == AppEngine.getInstance().constants.MESSAGE_ACTIVITY_RESULT_CODE_FILE && resultCode==RESULT_OK) {
            showProfilePicture.setImageBitmap(AppEngine.getInstance().storageImageHelper.getImagefromURI(this, data));
        }
        else if (requestCode == AppEngine.getInstance().constants.MESSAGE_ACTIVITY_RESULT_CODE_CAMERA && resultCode == Activity.RESULT_OK) {
            showProfilePicture.setImageBitmap(AppEngine.getInstance().storageImageHelper.getBitmapFromCamera(this, data));
        }else if (requestCode == AppEngine.getInstance().constants.RESULT_CODE_SELECT_COUNTRY && resultCode == Activity.RESULT_OK) {
            String countryId = data.getStringExtra("countryId");
            String countryName = data.getStringExtra("countryName");

            this.nationality.setText(countryName.toString());
            this.userInfo.setNationality(Integer.parseInt(countryId));
            this.nationality.setError(null);
        }
        uploadOptionHolder.setVisibility(View.INVISIBLE);
        AppEngine.getInstance().animation.slideUp(uploadOption, 500);
    }

    @Override
    public void onValidationSucceeded() {
        this.setData();
        this.registrationPresenter.registerUser(this.userInfo);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                ((EditText) view).setError(null);
            }
        }
        if(!this.password.getText().equals(this.confirmPassword.getText())){
            this.confirmPassword.setError("Password not match!");
        }
    }

    @Override
    public void CountryCallBack(CountryEntity cEntity) {

    }
}

package com.data.uber.goods.driver.app.presenter.settings;


import android.content.Context;
import android.widget.Toast;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.model.settings.AddVehicle;
import com.data.uber.goods.driver.app.model.settings.CarType;
import com.data.uber.goods.driver.app.model.settings.UpdateVehicle;
import com.data.uber.goods.driver.app.service.network.VehicleRestService;
import com.data.uber.goods.driver.app.view.settings.VehicleViewInterface;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class VehiclePresenter {
    private Context context;
    private VehicleViewInterface vehicleViewInterface;

    public VehiclePresenter(Context context, VehicleViewInterface vehicleViewInterface){
        this.context = context;
        this.vehicleViewInterface = vehicleViewInterface;
    }

    public void getVehicleList(){
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(VehicleRestService.class, context).
                        getVehicleList(AppEngine.getInstance().sharedPrefUtils.getPref("driverId", context)),
                new Observer<AddVehicle>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(AddVehicle vehicle) {
                        vehicleViewInterface.vehicleCount(vehicle);
                    }
                });
    }

    public void getCarTypes(){
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(VehicleRestService.class, context).
                        getCarType(),
                new Observer<List<CarType>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(@NonNull List<CarType> carTypeList) {
                        vehicleViewInterface.loadCarTypeSpinnerData(carTypeList);
                    }
                });
    }

    public void addVehicle(AddVehicle vehicle){
       AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(VehicleRestService.class, context).
                        addVehicle(vehicle),
                new Observer<AddVehicle>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(@NonNull AddVehicle vehicle) {
                        Toast.makeText(context, "Vehicle has been added successfully!", Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void updateVehicle(UpdateVehicle vehicle){
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(VehicleRestService.class, context).
                        updateVehicle(vehicle),
                new Observer<AddVehicle>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(AddVehicle vehicle) {
                        Toast.makeText(context, "Vehicle has been updated successfully!", Toast.LENGTH_LONG).show();
                    }
                });
    }
}

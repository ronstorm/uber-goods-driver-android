package com.data.uber.goods.driver.app.service.network;

import com.data.uber.goods.driver.app.model.mytrips.AcceptTripResponse;
import com.data.uber.goods.driver.app.model.rating.Rating;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RatingRestService {
    @POST("v1/rating/")
    Observable<AcceptTripResponse> submitRating(@Body Rating rating);
}


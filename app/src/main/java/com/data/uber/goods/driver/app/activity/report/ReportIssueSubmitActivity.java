package com.data.uber.goods.driver.app.activity.report;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;


import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.activity.BaseUIController;
import com.data.uber.goods.driver.app.model.report.IssueCreateRequest;
import com.data.uber.goods.driver.app.model.report.IssueCreateResponse;
import com.data.uber.goods.driver.app.presenter.report.ReportSubmitPresenter;
import com.data.uber.goods.driver.app.view.report.ReportIssueSubmitInterface;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class ReportIssueSubmitActivity extends BaseUIController implements ReportIssueSubmitInterface {

    private ReportSubmitPresenter reportSubmitPresenter;
    private IssueCreateRequest issueCreateRequest;
    @InjectView(R.id.reportIssueField)
    public EditText reportIssueField;
    @InjectView(R.id.cancelButton)
    public Button cancelButton;
    @InjectView(R.id.submitReport)
    public Button submitReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.DRAWER_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.activity_report_problem_form);
        this.init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {
        ButterKnife.inject(this);
        reportSubmitPresenter = new ReportSubmitPresenter(this, this);
        issueCreateRequest = new IssueCreateRequest();
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            issueCreateRequest.setTopic(Integer.parseInt(bundle.getString("topic")));
        }
    }

    @OnClick(R.id.cancelButton)
    public void onClickCancelButton() {
        finish();
    }
    @OnClick(R.id.submitReport)
    public void onClickSubmitReport() {
        showloading();
        issueCreateRequest.setDetails(reportIssueField.getText().toString());
        reportSubmitPresenter.submitIssue(issueCreateRequest);
    }

    @Override
    public void onSubmitApiSuccess(IssueCreateResponse issueCreateResponse) {
        hideloading();
        finish();
    }

    @Override
    public void onSubmitApiError() {
        hideloading();
    }
}

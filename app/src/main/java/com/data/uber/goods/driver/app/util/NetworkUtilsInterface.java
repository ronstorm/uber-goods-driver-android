package com.data.uber.goods.driver.app.util;

public interface NetworkUtilsInterface {
    public void networkDisconnect();
    public void networkConnect();
}

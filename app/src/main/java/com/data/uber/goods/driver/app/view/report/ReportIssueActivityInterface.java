package com.data.uber.goods.driver.app.view.report;


import com.data.uber.goods.driver.app.model.report.IssueCategory;

import java.util.List;


public interface ReportIssueActivityInterface {
    void onIssueListAndCategoryFetched(List<IssueCategory> issueCategories);
    void onIssueListAndCategoryFetchError();
}

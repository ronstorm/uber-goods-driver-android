package com.data.uber.goods.driver.app.view.settings;

import com.data.uber.goods.driver.app.model.settings.CarType;
import com.data.uber.goods.driver.app.model.settings.AddVehicle;

import java.util.List;

public interface VehicleViewInterface {
    void loadCarTypeSpinnerData(List<CarType> carTypeList);
    void vehicleCount(AddVehicle vehicle);
}

package com.data.uber.goods.driver.app.util;


import android.location.Location;

public class Constants {
    public final String SHARED_PREF = "wellTravelSharedPref";


    public final String EMAIL_PATTERN = "[a-zA-Z0-9_!#$%&'*+/=?`{|}~^]+[.-]?[a-zA-Z0-9]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+$";
    public final String PHONENUMBER_PATTERN = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
    public final String PASSWORD_PATTERN = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
    public final int LOGIN_EMPTY_FIELD = 1;

    //layout type
    public final String DRAWER_LAYOUT = "drawerLayout";
    public final String TOPBAR_LAYOUT = "topBarLayout";
    public final String EMPTY_LAYOUT = "emptyLayout";

    public final String MESSAGE_SOCKET_IO_HOST = "";
    public final String INTENT_DATA_IMAGE = "data";


    public final String API_BASE_URL = "https://ubergoods.herokuapp.com/api/";
    public final String GOOGLE_API_KEY = ""; //Please put your google api key here

    public final int MESSAGE_ACTIVITY_RESULT_CODE_CAMERA = 1231;
    public final int MESSAGE_ACTIVITY_RESULT_CODE_FILE = 123;
    public final int RESULT_CODE_SELECT_COUNTRY = 9090;

    public final String GOOGLE_MAP_GEO_CODING_BASE_URL = "https://maps.googleapis.com";
    public String CURRENT_LOCATION = "";

    public String ACCESS_TOKEN = "";

    //OAuth
    public final String OAUTH_CLIENT_ID = "FwzZFbyFK0u1u6UNCFRCydm6jMuAC13zbWKWt9Hw";
    public final String OAUTH_CLIENT_SECRET = "fceFVxfLtM6mvgKmJwFQNBhHnxjCyYwMw8y8cBVIC4PgAsrwhYFNTHxcDC5H7GW6RViSv317r0eSEogvzi5J16V040pNAjS8OWwchz3c5yD6LSUCJTFoUePcv6sNDWTs";
    public String OAUTH_ACCESS_TOKEN = "access_token";
    public String OAUTH_EXPIRES_IN = "expires_in";
    public String OAUTH_TOKEN_TYPE = "token_type";
    public String OAUTH_REFRESHT_GRANT_TYPE = "refresh_token";
    public String OAUTH_SCOPE = "scope";
    public String OAUTH_REFRESH_TOKEN = "refresh_token";
    public boolean loggenIn = false;
    //OAuth Ends

    //forget password
    public Integer forgetCode = null;
    public String forgetEmail = "";

    //location
    public Location CURRENT_LATLNG = null;

    //driver
    public String DRIVER_ID = "";

    public float ROTATION = 0.0f;
    public Location LOCATION;
}

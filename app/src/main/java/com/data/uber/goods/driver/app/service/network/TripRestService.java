package com.data.uber.goods.driver.app.service.network;


import com.data.uber.goods.driver.app.model.mytrips.DriverProfile;
import com.data.uber.goods.driver.app.model.mytrips.TripItem;

import java.util.List;

import retrofit2.http.GET;
import io.reactivex.Observable;
import retrofit2.http.Path;

/**
 * Created by kud-wtag on 6/24/18.
 */

public interface TripRestService {
    @GET("/api/v1/trip/")
    Observable<List<TripItem>> getAllTrips();

    @GET("/api/v1/recurring-trip/")
    Observable<List<TripItem>> getAllRecurringTrips();

    @GET("/api/v1/customer/{id}/")
    Observable<DriverProfile> getCustomerProfileInfo(@Path("id") String id);
}

package com.data.uber.goods.driver.app.presenter.mytrip;

import android.content.Context;
import android.widget.Toast;


import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.model.mytrips.DriverProfile;
import com.data.uber.goods.driver.app.model.mytrips.TripItem;
import com.data.uber.goods.driver.app.service.network.TripRestService;
import com.data.uber.goods.driver.app.view.mytrip.MyTripsInterface;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

/**
 * Created by kud-wtag on 6/24/18.
 */

public class MytripsPresenter {
    private Context context;
    private MyTripsInterface myTripsInterface;
    private List<TripItem> tripItemListPrivate;

    public MytripsPresenter(Context context, MyTripsInterface myTripsInterface) {
        this.context = context;
        this.myTripsInterface = myTripsInterface;
    }

    public void getAllTrips() {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(TripRestService.class, context).getAllTrips(),
                new Observer<List<TripItem>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull List<TripItem> tripItemList) {

                        tripItemListPrivate = tripItemList;
                        for(int i=0; i<tripItemList.size(); i++) {
                            getCustomerProfile(tripItemList.get(i).getCustomer(), tripItemListPrivate.get(i));
                        }
                        myTripsInterface.tripsFetchSuccess(tripItemListPrivate);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        myTripsInterface.tripsFetchError();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getCustomerProfile(String driverId, final TripItem tripItem) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(TripRestService.class, context).
                        getCustomerProfileInfo(driverId),
                new Observer<DriverProfile>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                        //Toast.makeText(context, "Error " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(@NonNull DriverProfile userInfo) {
                        tripItem.setDriverProfile(userInfo);
                        myTripsInterface.tripsFetchSuccess(tripItemListPrivate);
                    }
                });
    }
}

package com.data.uber.goods.driver.app.model.report;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IssueCreateRequest {
    @SerializedName("topic")
    @Expose
    private Integer topic;
    @SerializedName("details")
    @Expose
    private String details;

    public Integer getTopic() {
        return topic;
    }

    public void setTopic(Integer topic) {
        this.topic = topic;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}

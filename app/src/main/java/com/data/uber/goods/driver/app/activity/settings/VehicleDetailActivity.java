package com.data.uber.goods.driver.app.activity.settings;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.Spinner;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.activity.BaseUIController;
import com.data.uber.goods.driver.app.model.settings.AddVehicle;
import com.data.uber.goods.driver.app.model.settings.CarType;
import com.data.uber.goods.driver.app.model.settings.UpdateVehicle;
import com.data.uber.goods.driver.app.presenter.settings.VehiclePresenter;
import com.data.uber.goods.driver.app.view.settings.VehicleViewInterface;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class VehicleDetailActivity extends BaseUIController implements VehicleViewInterface{

    @InjectView(R.id.modelName)
    EditText modelName;

    Spinner carType;

    @InjectView(R.id.registrationYear)
    EditText registrationYear;

    @InjectView(R.id.registrationNumber)
    EditText registrationNumber;

    @InjectView(R.id.chassisNumber)
    EditText chassisNumber;

    @InjectView(R.id.insuranceNumber)
    EditText insuranceNumber;

    @InjectView(R.id.maxCapacity)
    EditText maxCapacity;

    @InjectView(R.id.allowedWeight)
    EditText allowedWeight;

    private VehiclePresenter vehiclePresenter;
    public static List<CarType> staticCarTypeList;
    public static int vehicleCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.DRAWER_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.activity_vehicle_detail);
        ButterKnife.inject(this);
        vehiclePresenter = new VehiclePresenter(this, this);

        carType = (Spinner) findViewById(R.id.carType);

        vehiclePresenter.getCarTypes();
        vehiclePresenter.getVehicleList();
        vehiclePresenter.getVehicleList();
    }

    @OnClick(R.id.addVehicleButton)
    public void clickSubmitButton(){
        if(vehicleCount ==0){
            AddVehicle vehicle = new AddVehicle();
            vehicle.setModelName(modelName.getText().toString());

            for(CarType carType: staticCarTypeList){
                if(carType.getName().equals(this.carType.getSelectedItem().toString())){
                    vehicle.setCarType(carType.getId());
                }
            }

            vehicle.setRegistrationNumber(registrationNumber.getText().toString());
            vehicle.setChassisNumber(chassisNumber.getText().toString());
            vehicle.setInsuranceNumber(insuranceNumber.getText().toString());
            vehicle.setMaxCapacity(Integer.parseInt(maxCapacity.getText().toString()));
            vehicle.setAllowedCapacity(Integer.parseInt(allowedWeight.getText().toString()));
            vehiclePresenter.addVehicle(vehicle);
        }else{
            UpdateVehicle vehicle = new UpdateVehicle();
            vehicle.setModelName(modelName.getText().toString());

            for(CarType carType: staticCarTypeList){
                if(carType.getName().equals(this.carType.getSelectedItem().toString())){
                    vehicle.setCarType(carType.getId());
                }
            }

            vehicle.setRegistrationNumber(registrationNumber.getText().toString());
            vehicle.setChassisNumber(chassisNumber.getText().toString());
            vehicle.setInsuranceNumber(insuranceNumber.getText().toString());
            vehicle.setMaxCapacity(Integer.parseInt(maxCapacity.getText().toString()));
            vehicle.setAllowedCapacity(Integer.parseInt(allowedWeight.getText().toString()));
            vehiclePresenter.updateVehicle(vehicle);
        }

    }

    @Override
    public void loadCarTypeSpinnerData(List<CarType> carTypeList) {
        staticCarTypeList = carTypeList;
        CarTypeListAdapter carTypeListAdapter = new CarTypeListAdapter(this, carTypeList);
        carType.setAdapter(carTypeListAdapter);
    }

    @Override
    public void vehicleCount(AddVehicle vehicle) {
        if(vehicle == null){
            vehicleCount = 0;
        }else{
            vehicleCount = 1;
            modelName.setText(vehicle.getModelName());
            registrationNumber.setText(vehicle.getRegistrationNumber());
            chassisNumber.setText(vehicle.getChassisNumber());
            insuranceNumber.setText(vehicle.getInsuranceNumber());
            maxCapacity.setText(String.valueOf(vehicle.getMaxCapacity()));
            allowedWeight.setText(String.valueOf(vehicle.getAllowedCapacity()));
        }
    }

    @OnClick(R.id.addVehicleCancelBtn)
    public void onClickCancelButton() {
        finish();
    }
}

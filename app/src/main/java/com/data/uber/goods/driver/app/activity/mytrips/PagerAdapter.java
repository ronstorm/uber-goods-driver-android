package com.data.uber.goods.driver.app.activity.mytrips;

/**
 * Created by kud-wtag on 6/24/18.
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Context context;

    public PagerAdapter(FragmentManager fm, int NumOfTabs, Context context) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                ImidiateTripFragment tab1 = new ImidiateTripFragment();
                tab1.initialize(context);
                return tab1;
            case 1:
                RecurringTripFrament tab3 = new RecurringTripFrament();
                tab3.initialize(context);
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

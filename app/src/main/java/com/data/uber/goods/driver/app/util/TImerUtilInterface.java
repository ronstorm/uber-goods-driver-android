package com.data.uber.goods.driver.app.util;

public interface TImerUtilInterface {
    public void executeTask();
    public void taskCompleted();
}

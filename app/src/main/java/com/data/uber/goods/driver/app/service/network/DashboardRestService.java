package com.data.uber.goods.driver.app.service.network;

import android.app.Activity;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.activity.dashboard.DashBoardActivity;
import com.data.uber.goods.driver.app.model.authentication.ResetPassword;
import com.data.uber.goods.driver.app.model.authentication.ResetPasswordRespose;
import com.data.uber.goods.driver.app.model.authentication.UserInfo;
import com.data.uber.goods.driver.app.model.mytrips.AcceptTrip;
import com.data.uber.goods.driver.app.model.mytrips.AcceptTripResponse;
import com.data.uber.goods.driver.app.model.mytrips.CustomerProfile;
import com.data.uber.goods.driver.app.model.mytrips.DriverProfile;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DashboardRestService {
    @GET("v1/driver/profile/")
    Observable<UserInfo> getDriverProfileInfo();

    @POST("v1/trip/{trip_id}/accept/")
    Observable<AcceptTripResponse> acceptRequest(@Body AcceptTrip acceptTrip, @Path("trip_id") String tripId);

    @POST("v1/trip/{id}/ontheway/")
    Observable<AcceptTripResponse> onTheWay(@Body AcceptTrip acceptTrip, @Path("id") String tripId);

    @POST("v1/trip/{id}/start/")
    Observable<AcceptTripResponse> startTrip(@Body AcceptTrip acceptTrip, @Path("id") String tripId);

    @POST("v1/trip/{id}/end/")
    Observable<AcceptTripResponse> endTrip(@Body AcceptTrip acceptTrip, @Path("id") String tripId);

    @DELETE("v1/trip/{id}/")
    Observable<AcceptTripResponse> rejectTrip(@Path("id") String tripId);

    @GET("/maps/api/directions/json")
    Observable<String> getAddressFromLatLng(@Query("origin") String origin, @Query("destination") String destination, @Query("key") String key);

    @GET("/api/v1/customer/{id}/")
    Observable<CustomerProfile> getCustomerProfileInfo(@Path("id") String id);
}

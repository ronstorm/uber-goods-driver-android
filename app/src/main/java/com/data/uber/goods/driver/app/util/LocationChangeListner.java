package com.data.uber.goods.driver.app.util;

import android.location.Location;

/**
 * Created by kud-wtag on 3/8/18.
 */

public interface LocationChangeListner {
    void onLocationChangedApp(Location location);
}

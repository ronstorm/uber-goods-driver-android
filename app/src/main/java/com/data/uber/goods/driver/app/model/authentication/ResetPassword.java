package com.data.uber.goods.driver.app.model.authentication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kud-wtag on 6/12/18.
 */

public class ResetPassword {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("new_password")
    @Expose
    private String newPassword;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}

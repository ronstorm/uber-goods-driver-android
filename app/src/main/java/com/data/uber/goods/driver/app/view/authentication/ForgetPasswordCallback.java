package com.data.uber.goods.driver.app.view.authentication;

public interface ForgetPasswordCallback {
    public void success();
    public void error();
}

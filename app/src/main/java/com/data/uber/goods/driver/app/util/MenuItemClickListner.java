package com.data.uber.goods.driver.app.util;

public interface MenuItemClickListner {
    public void onMenuItemClick();
}

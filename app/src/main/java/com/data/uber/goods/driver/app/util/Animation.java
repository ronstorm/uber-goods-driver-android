package com.data.uber.goods.driver.app.util;

import android.content.Context;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;

import com.data.uber.goods.driver.app.AppEngine;


public class Animation {

    private float viewAlpha;
    private boolean initFadeing;
    private boolean fading;

    public Animation() {
        viewAlpha = 0;
        initFadeing = false;
    }

    public void slideUp(View view, int duration){
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(duration);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }
    public void slideDown(View view, int duration){
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(duration);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    public void fadeAway(final Context context, final View view) {
        AppEngine.getInstance().timerUtil.stoptimertask();
        if(initFadeing) {
            fading = true;
            viewAlpha = 1;
            view.setAlpha(1);
            view.setVisibility(View.VISIBLE);
            initFadeing = !initFadeing;
        }
        AppEngine.getInstance().timerUtil.initTimeCustomTask(context, 1, new TImerUtilInterface() {
            @Override
            public void executeTask() {
                if(viewAlpha>0) {
                    view.setAlpha(viewAlpha);
                    viewAlpha -= 0.05;
                    fadeAway(context, view);
                } else {
                    view.setAlpha(0);
                    viewAlpha = 0;
                    initFadeing = false;
                    view.setVisibility(View.INVISIBLE);
                    fading = false;
                    AppEngine.getInstance().timerUtil.stoptimertask();
                }
            }

            @Override
            public void taskCompleted() {

            }
        });
    }

    public void fadeIn(final Context context, final View view) {
        AppEngine.getInstance().timerUtil.stoptimertask();
        if(initFadeing) {
            fading = true;
            viewAlpha = 0;
            view.setAlpha(0);
            view.setVisibility(View.VISIBLE);
            initFadeing = !initFadeing;
        }
        AppEngine.getInstance().timerUtil.initTimeCustomTask(context, 1, new TImerUtilInterface() {
            @Override
            public void executeTask() {
                if(viewAlpha<1) {
                    view.setAlpha(viewAlpha);
                    viewAlpha += 0.05;
                    fadeIn(context, view);
                } else {
                    view.setAlpha(1);
                    viewAlpha = 1;
                    initFadeing = false;
                    view.setVisibility(View.VISIBLE);
                    fading = false;
                    AppEngine.getInstance().timerUtil.stoptimertask();
                }
            }

            @Override
            public void taskCompleted() {

            }
        });
    }

    public void setViewAlpha(float viewAlpha) {
        this.viewAlpha = viewAlpha;
    }

    public boolean isFading() {
        return fading;
    }

    public void setFading(boolean fading) {
        this.fading = fading;
    }

    public void setInitFadeing(boolean initFadeing) {
        this.initFadeing = initFadeing;
    }
}

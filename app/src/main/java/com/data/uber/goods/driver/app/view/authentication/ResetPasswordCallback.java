package com.data.uber.goods.driver.app.view.authentication;

import com.data.uber.goods.driver.app.model.authentication.ResetPasswordRespose;

public interface ResetPasswordCallback {
    public void success(ResetPasswordRespose resetPasswordRespose);
    public void error();
}

package com.data.uber.goods.driver.app.util;

public class MenuItem {
    String title;
    int imageResId;
    Class activityClass;
    int layoutType;
    MenuItemClickListner menuItemClickListner;
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageResId() {
        return imageResId;
    }

    public void setImageResId(int imageResId) {
        this.imageResId = imageResId;
    }

    public Class getActivityClass() {
        return activityClass;
    }

    public void setActivityClass(Class activityClass) {
        this.activityClass = activityClass;
    }

    public int getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(int layoutType) {
        this.layoutType = layoutType;
    }

    public void setMenuItemClickListner(MenuItemClickListner menuItemClickListner) {
        this.menuItemClickListner = menuItemClickListner;
    }

    public MenuItemClickListner getMenuItemClickListner() {
        return menuItemClickListner;
    }
}

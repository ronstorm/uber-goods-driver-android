package com.data.uber.goods.driver.app.service.network;



import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.BuildConfig;
import com.data.uber.goods.driver.app.model.authentication.LoginResponse;
import com.data.uber.goods.driver.app.model.notification.FirebaseRegistration;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface LoginService {
    @FormUrlEncoded
    @POST("/o/token/?profile_type=Driver")
    @Headers({
            "Authorization: Basic Rnd6WkZieUZLMHUxdTZVTkNGUkN5ZG02ak11QUMxM3piV0tXdDlIdzpmY2VGVnhmTHRNNm12Z0ttSndGUU5CaEhueGpDeVl3TXc4eThjQlZJQzRQZ0FzcndoWUZOVEh4Y0RDNUg3R1c2UlZpU3YzMTdyMGVTRW9ndnppNUoxNlYwNDBwTkFqUzhPV3djaHozYzV5RDZMU1VDSlRGb1VlUGN2NnNORFdUcw=="
    })
    Observable<LoginResponse> login(@Field("username") String username, @Field("password") String password, @Field("grant_type") String grantType);

    @FormUrlEncoded
    @POST("/o/token/")
    Call<LoginResponse> refreshToken(@Field("refresh_token") String refreshToken, @Field("client_id") String clientId, @Field("client_secret") String clientSecret, @Field("grant_type") String grantType);

    @POST("/api/v1/fcm/")
    Observable<FirebaseRegistration> fcmRegistration(@Body FirebaseRegistration firebaseRegistration);
}
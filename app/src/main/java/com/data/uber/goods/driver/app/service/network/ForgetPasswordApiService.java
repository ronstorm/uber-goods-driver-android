package com.data.uber.goods.driver.app.service.network;


import com.data.uber.goods.driver.app.model.authentication.ForgetPassword;
import com.data.uber.goods.driver.app.model.authentication.ResetPassword;
import com.data.uber.goods.driver.app.model.authentication.ResetPasswordRespose;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ForgetPasswordApiService {

    @POST("/api/v1/account/forgot-password/")
    Observable<Response<String>> forgetPassword(@Body ForgetPassword forgetPassword);

    @POST("/api/v1/account/reset-password/")
    Observable<ResetPasswordRespose> resetPassword(@Body ResetPassword resetPassword);
}

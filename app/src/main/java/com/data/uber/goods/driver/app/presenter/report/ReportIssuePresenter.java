package com.data.uber.goods.driver.app.presenter.report;

import android.content.Context;
import android.widget.Toast;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.model.report.IssueCategory;
import com.data.uber.goods.driver.app.service.network.IssueApiService;
import com.data.uber.goods.driver.app.view.report.ReportIssueActivityInterface;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;



public class ReportIssuePresenter {
    ReportIssueActivityInterface reportIssueActivityInterface;
    Context context;

    public ReportIssuePresenter(ReportIssueActivityInterface reportIssueActivityInterface, Context context) {
        this.reportIssueActivityInterface = reportIssueActivityInterface;
        this.context = context;
    }

    public void getIssueListAndCategory() {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(IssueApiService.class, context).getIssueCategoryAndTopics(),
                new Observer<List<IssueCategory>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<IssueCategory> issueCategories) {
                        reportIssueActivityInterface.onIssueListAndCategoryFetched(issueCategories);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                        reportIssueActivityInterface.onIssueListAndCategoryFetchError();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}

package com.data.uber.goods.driver.app.model.mytrips;

import java.io.Serializable;

public class MyTrip implements Serializable{
    private String name;

    public MyTrip(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

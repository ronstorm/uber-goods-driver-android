package com.data.uber.goods.driver.app.activity.settings;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.activity.BaseUIController;

import java.util.ArrayList;
import java.util.List;



public class SettingsActivity extends BaseUIController implements View.OnClickListener{

    ListView settingsLanguageList;
    ImageView vehicleEditButton;
    ImageButton backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.TOPBAR_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.activity_settings_screen);
        this.init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {
        this.vehicleEditButton = findViewById(R.id.vehicleEditButton);
        this.vehicleEditButton.setOnClickListener(this);
        this.backBtn = findViewById(R.id.moduleTopBarBackBtn);
        this.backBtn.setOnClickListener(this);
        this.setNavBarTitle("Settings");
        settingsLanguageList = (ListView) findViewById(R.id.settingsLanguageList);
        List<String> languageList = new ArrayList<>();
        languageList.add("Arabic");
        languageList.add("English");
        SettingsLanguageListAdapter settingsLanguageListAdapter = new SettingsLanguageListAdapter(this, languageList);
        settingsLanguageList.setAdapter(settingsLanguageListAdapter);
    }

    @Override
    public void onClick(View view) {
        if(view.equals(this.vehicleEditButton)){
            AppEngine.getInstance().androidIntent.startActivity(this, VehicleDetailActivity.class);
        }
        else if(view.equals(this.backBtn)){
            this.onBackPressed();
        }
    }
}

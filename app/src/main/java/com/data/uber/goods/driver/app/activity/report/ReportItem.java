package com.data.uber.goods.driver.app.activity.report;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bitto on 10-Apr-18.
 */

public class ReportItem {
    private String item;
    private List<ReportItem> reportItems;

    public ReportItem() {
        reportItems = new ArrayList<>();
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public List<ReportItem> getReportItems() {
        return reportItems;
    }

    public void setReportItems(List<ReportItem> reportItems) {
        this.reportItems = reportItems;
    }
}

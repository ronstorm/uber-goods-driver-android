package com.data.uber.goods.driver.app.activity.dashboard;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.activity.BaseUIController;
import com.data.uber.goods.driver.app.model.authentication.UserInfo;
import com.data.uber.goods.driver.app.model.mytrips.AcceptTrip;
import com.data.uber.goods.driver.app.model.mytrips.CustomerProfile;
import com.data.uber.goods.driver.app.model.rating.Rating;
import com.data.uber.goods.driver.app.presenter.dashboard.DashboardPresenter;
import com.data.uber.goods.driver.app.util.GmapView;
import com.data.uber.goods.driver.app.util.GoogleMapLocationListner;
import com.data.uber.goods.driver.app.util.TImerUtilInterface;
import com.data.uber.goods.driver.app.view.dashboard.DashboardInterface;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class DashBoardActivity extends BaseUIController implements GoogleMapLocationListner, DashboardInterface, GoogleMap.OnMapLoadedCallback{

    @InjectView(R.id.acceptRequestFromCustomer)
    LinearLayout acceptRequestFromCustomer;

    @InjectView(R.id.startTrip)
    LinearLayout startTrip;

    @InjectView(R.id.endTrip)
    LinearLayout endTrip;

    @InjectView(R.id.onTheWay)
    LinearLayout onTheWay;

    @InjectView(R.id.tripPayment)
    ScrollView tripPayment;

    @InjectView(R.id.userDashboardName)
    TextView userDashboardName;

    @InjectView(R.id.ratingSubmitBtn)
    Button submitBtn;

    @InjectView(R.id.ratingComment)
    EditText ratingComment;

    @InjectView(R.id.ratingBar)
    RatingBar ratingBar;

    @InjectView(R.id.sourceLocation)
    TextView sourceLocation;

    @InjectView(R.id.destinationLocation)
    TextView destinationLocation;

    @InjectView(R.id.txtNameOnTheWay)
    TextView txtNameOnTheWay;

    @InjectView(R.id.txtNameStartTrip)
    TextView txtNameStartTrip;

    @InjectView(R.id.txtNameEndTrip)
    TextView txtNameEndTrip;

    @InjectView(R.id.customerImageOnTheWay)
    ImageView customerImageOnTheWay;

    @InjectView(R.id.driverImage)
    ImageView driverImage;

    @InjectView(R.id.customerImageStartTrip)
    ImageView customerImageStartTrip;

    @InjectView(R.id.customerImageEndTrip)
    ImageView customerImageEndTrip;

    @InjectView(R.id.customerImageRating)
    ImageView customerImageRating;

    @InjectView(R.id.customerNameRating)
    TextView  customerNameRating;

    @InjectView(R.id.amountRating)
    TextView  amountRating;

    @InjectView(R.id.paymentType)
    TextView  paymentType;

    boolean driverFound = false;
    GmapView gmapView;
    private DashboardPresenter dashboardPresenter;
    BroadcastReceiver receiver;

    double tripPickup[];
    double tripDest[];

    PolylineOptions lineOptions;
    Location location;
    Location geoLocation;
    float markerRotation;

    Marker marker;
    boolean dragCamera = true;
    int tripState = 0;
    Polyline polyline;

    Marker pickUpMarker;
    Marker destinationMarker;

    boolean driverLocationInit = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.DRAWER_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.activity_dashboard);
        this.init(savedInstanceState);

    }

    private void tripStateCheck() {
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null && bundle.getString("tripId")!=null && !bundle.getString("tripId").isEmpty()){
            if(bundle.getString("id").equals("7")) {
                AppEngine.getInstance().sharedPrefUtils.putPref("amount", bundle.getString("amount"), this);
                AppEngine.getInstance().sharedPrefUtils.putPref("medium", bundle.getString("medium"), this);
                AppEngine.getInstance().animation.slideUp(tripPayment, 500);
                AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "paymentRcv", this);
            } else {
                AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "acceptRequest", this);
                AppEngine.getInstance().sharedPrefUtils.putPref("tripId", bundle.getString("tripId"), this);

                AppEngine.getInstance().sharedPrefUtils.putPref("pickUp", bundle.getString("pickUp"), this);
                AppEngine.getInstance().sharedPrefUtils.putPref("destination", bundle.getString("destination"), this);

                AppEngine.getInstance().sharedPrefUtils.putPref("pickUpStr", bundle.getString("pickUpStr"), this);
                AppEngine.getInstance().sharedPrefUtils.putPref("destinationStr", bundle.getString("destinationStr"), this);
                AppEngine.getInstance().sharedPrefUtils.putPref("customerId", bundle.getString("customerId"), this);
            }
        }
        if(AppEngine.getInstance().sharedPrefUtils.getPref("tripState", this).equals("acceptRequest")){
            tripState = 1;
            this.sourceLocation.setText(AppEngine.getInstance().sharedPrefUtils.getPref("pickUpStr", this));
            this.destinationLocation.setText(AppEngine.getInstance().sharedPrefUtils.getPref("destinationStr", this));
            AppEngine.getInstance().animation.slideUp(acceptRequestFromCustomer, 500);
        }else if(AppEngine.getInstance().sharedPrefUtils.getPref("tripState", this).equals("onTheWay")){
            tripState = 2;
            dashboardPresenter.getCustomerProfile(AppEngine.getInstance().sharedPrefUtils.getPref("customerId", this));
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            AppEngine.getInstance().animation.slideUp(onTheWay, 500);
        }else if(AppEngine.getInstance().sharedPrefUtils.getPref("tripState", this).equals("startTrip")){
            tripState = 3;
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            dashboardPresenter.getCustomerProfile(AppEngine.getInstance().sharedPrefUtils.getPref("customerId", this));
            AppEngine.getInstance().animation.slideUp(startTrip, 500);
        }else if(AppEngine.getInstance().sharedPrefUtils.getPref("tripState", this).equals("endTrip")){
            tripState = 4;
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            dashboardPresenter.getCustomerProfile(AppEngine.getInstance().sharedPrefUtils.getPref("customerId", this));
            AppEngine.getInstance().animation.slideUp(endTrip, 500);
        }else if(AppEngine.getInstance().sharedPrefUtils.getPref("tripState", this).equals("paymentRcv")){
            tripState = 0;
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            dashboardPresenter.getCustomerProfile(AppEngine.getInstance().sharedPrefUtils.getPref("customerId", this));
            this.amountRating.setText("SAR " + AppEngine.getInstance().sharedPrefUtils.getPref("amount", this));
            if(AppEngine.getInstance().sharedPrefUtils.getPref("medium", this).equals("0")){
                this.paymentType.setText("Collect Cash");
            }else{
                this.paymentType.setText("Card");
            }
            AppEngine.getInstance().animation.slideUp(tripPayment, 500);
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                onReceived(context, intent);
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver), new IntentFilter("NotificationReceived"));
    }

    public void onReceived(Context context, Intent intent){
        int tripState = intent.getExtras().getInt("tripState");
        if(tripState == 0){
            this.sourceLocation.setText(AppEngine.getInstance().sharedPrefUtils.getPref("pickUpStr", this));
            this.destinationLocation.setText(AppEngine.getInstance().sharedPrefUtils.getPref("destinationStr", this));
            dashboardPresenter.getCustomerProfile(AppEngine.getInstance().sharedPrefUtils.getPref("customerId", this));
            AppEngine.getInstance().animation.slideUp(acceptRequestFromCustomer, 500);

        }
        if(tripState == 7){
            this.amountRating.setText("SR " + AppEngine.getInstance().sharedPrefUtils.getPref("amount", this));
            if(AppEngine.getInstance().sharedPrefUtils.getPref("medium", this).equals("0")){
                this.paymentType.setText("Collect Cash");
            }else{
                this.paymentType.setText("Card");
            }
            dashboardPresenter.getCustomerProfile(AppEngine.getInstance().sharedPrefUtils.getPref("customerId", this));
            AppEngine.getInstance().animation.slideUp(tripPayment, 500);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean dragCamera = true;
        AppEngine.getInstance().googleMapView.setGoogleMapLocationListner(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void init(Bundle savedInstanceState) {
        ButterKnife.inject(this);
        gmapView = new GmapView();
        gmapView.setLoadCustomMarker(true);
        gmapView.initView(this, (MapView) findViewById(R.id.mapView), savedInstanceState, true);
        gmapView.getMapView().onStart();
        AppEngine.getInstance().googleMapView.setGoogleMapLocationListner(this);
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        this.dashboardPresenter = new DashboardPresenter(this, this);
        this.dashboardPresenter.getDriverProfile();
        this.tripStateCheck();

        tripDest = new double[2];
        tripPickup = new double[2];

        final Context context = this;
        Runnable driverLocationUpdate = new Runnable() {
            @Override
            public void run() {
                while(true) {
                    try {
                        Thread.sleep(20000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(tripState > 1 &&
                            !AppEngine.getInstance().sharedPrefUtils.getPref("tripState", context).equals("acceptRequest") &&
                            !AppEngine.getInstance().sharedPrefUtils.getPref("tripState", context).equals("onTheWay")) {
                        if(location!=null) dashboardPresenter.updateDriverLocation(location, markerRotation);
                    }
                }
            }
        };

        Thread locationUpdate = new Thread(driverLocationUpdate);
        locationUpdate.start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        AppEngine.getInstance().googleMapView.requestPermissionCallback(requestCode, grantResults, this);

        //sliderUpAndDown();

    }

    @Override
    public void setUserDetails(UserInfo userInfo) {
        this.userDashboardName.setText(userInfo.getUser().getName());
        Picasso.with(this).load(userInfo.getUser().getPicture()).into(this.driverImage);
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

    @OnClick(R.id.requestAcceptBtn)
    public void acceptRequestbtnClicked(){
        if(AppEngine.getInstance().constants.CURRENT_LATLNG!=null) {
            AcceptTrip acceptTrip = new AcceptTrip(true);
            com.data.uber.goods.driver.app.model.mytrips.Location location = new com.data.uber.goods.driver.app.model.mytrips.Location();
            location.setLatitude(AppEngine.getInstance().constants.CURRENT_LATLNG.getLatitude());
            location.setLongitude(AppEngine.getInstance().constants.CURRENT_LATLNG.getLongitude());
            Gson gson = new Gson();
            acceptTrip.setLocation(gson.toJson(location));
            Log.d("tripRequest", gson.toJson(acceptTrip));
            dashboardPresenter.acceptRequest(acceptTrip);
            AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "onTheWay", this);
        } else {
            Toast.makeText(this, "Waiting For Your Location", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.onTheWayBtn)
    public void onWayBtnClicked(){
        dashboardPresenter.onTheWay(new AcceptTrip(true));
        AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "startTrip", this);
    }

    @OnClick(R.id.startTripBtn)
    public void startTripBtnClicked(){
        if(AppEngine.getInstance().constants.CURRENT_LATLNG!=null) {
            AcceptTrip acceptTrip = new AcceptTrip(true);
            com.data.uber.goods.driver.app.model.mytrips.Location location = new com.data.uber.goods.driver.app.model.mytrips.Location();
            location.setLatitude(AppEngine.getInstance().constants.CURRENT_LATLNG.getLatitude());
            location.setLongitude(AppEngine.getInstance().constants.CURRENT_LATLNG.getLongitude());
            Gson gson = new Gson();
            acceptTrip.setLocation(gson.toJson(location));
            dashboardPresenter.startTrip(acceptTrip);
        }else{
            Toast.makeText(this, "Waiting For Your Location", Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.endTripBtn)
    public void endTripBtnClicked(){
        if(AppEngine.getInstance().constants.CURRENT_LATLNG!=null) {
            AcceptTrip acceptTrip = new AcceptTrip(true);
            com.data.uber.goods.driver.app.model.mytrips.Location location = new com.data.uber.goods.driver.app.model.mytrips.Location();
            location.setLatitude(this.location.getLatitude());
            location.setLongitude(this.location.getLongitude());
            Gson gson = new Gson();
            acceptTrip.setLocation(gson.toJson(location));
            dashboardPresenter.endTrip(acceptTrip);
            AppEngine.getInstance().sharedPrefUtils.clearPref("tripState", this);
        } else {
            Toast.makeText(this, "Waiting For Your Location", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.requestRejectBtn)
    public void cancelRequestBtnClicked(){
        cancelTripAnyState();
    }

    @OnClick(R.id.onTheWayCancelBtn)
    public void onTheWaycancelBtnClicked(){
        cancelTripAnyState();
    }

    @OnClick(R.id.startTripCancelBtn)
    public void startTripcancelBtnClicked(){
        cancelTripAnyState();
    }

    @OnClick(R.id.endTripCancelBtn)
    public void endTripcancelBtnClicked(){
        cancelTripAnyState();
    }

    @OnClick(R.id.ratingSubmitBtn)
    public void submitBtnOnClick() {
        Rating rating = new Rating();
        rating.setTrip(Integer.parseInt(AppEngine.getInstance().sharedPrefUtils.getPref("tripId", this)));
        rating.setComment(this.ratingComment.getText().toString());
        rating.setRating((int)ratingBar.getRating());
        this.dashboardPresenter.submitRating(rating);
    }

    public void cancelTripAnyState(){
        dashboardPresenter.rejectTrip();
    }

    @Override
    public void acceptRequest(){
        dashboardPresenter.getCustomerProfile(AppEngine.getInstance().sharedPrefUtils.getPref("customerId", this));
        AppEngine.getInstance().animation.slideDown(acceptRequestFromCustomer, 500);
        AppEngine.getInstance().animation.slideUp(onTheWay, 500);
        tripState = 2;
    }

    @Override
    public void onTheWay() {
        AppEngine.getInstance().animation.slideDown(onTheWay, 500);
        AppEngine.getInstance().animation.slideUp(startTrip, 500);
        tripState = 3;
    }

    @Override
    public void startTrip() {
        AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "endTrip", this);
        AppEngine.getInstance().animation.slideDown(startTrip, 500);
        AppEngine.getInstance().animation.slideUp(endTrip, 500);
        if(destinationMarker!=null) destinationMarker.remove();
        tripState = 4;
    }

    @Override
    public void endTrip() {
        AppEngine.getInstance().animation.slideDown(endTrip, 500);
        tripState = 0;
    }

    @Override
    public void rejectTripAnyState() {
        AppEngine.getInstance().animation.slideDown(acceptRequestFromCustomer, 500);
        AppEngine.getInstance().animation.slideDown(onTheWay, 500);
        AppEngine.getInstance().animation.slideDown(startTrip, 500);
        AppEngine.getInstance().animation.slideDown(endTrip, 500);
    }

    @Override
    public void setRating() {
        AppEngine.getInstance().animation.slideDown(tripPayment, 500);
        AppEngine.getInstance().sharedPrefUtils.putPref("tripState", "", this);
    }

    @Override
    public void getCustomerProfile(CustomerProfile customerProfile) {
        this.txtNameOnTheWay.setText(customerProfile.getUser().getName());
        this.txtNameStartTrip.setText(customerProfile.getUser().getName());
        this.txtNameEndTrip.setText(customerProfile.getUser().getName());
        Picasso.with(this).load(customerProfile.getUser().getPicture()).into(this.customerImageOnTheWay);
        Picasso.with(this).load(customerProfile.getUser().getPicture()).into(this.customerImageStartTrip);
        Picasso.with(this).load(customerProfile.getUser().getPicture()).into(this.customerImageEndTrip);
        Picasso.with(this).load(customerProfile.getUser().getPicture()).into(this.customerImageRating);
        this.customerNameRating.setText(customerProfile.getUser().getName());
    }

    @Override
    public void setDirection(List<List<HashMap<String, String>>> routes) {
        ArrayList<LatLng> points = null;
        lineOptions = null;
        MarkerOptions markerOptions = new MarkerOptions();

        if(routes!=null) {
            for(int i=0;i<routes.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = routes.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){

                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);


                    points.add(position);

                    if(i==0 && j==0) {
                        tripPickup[0] = Double.parseDouble(point.get("lat"));
                        tripPickup[1] = Double.parseDouble(point.get("lng"));
                    }
                    if(i==routes.size()-1 && j==path.size()-1) {
                        tripDest[0] = Double.parseDouble(point.get("lat"));
                        tripDest[1] = Double.parseDouble(point.get("lng"));
                    }
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(10);
                lineOptions.color(Color.BLACK);
            }

            Log.d("pickup",AppEngine.getInstance().sharedPrefUtils.getPref("pickUp", this));
            Log.d("dest", AppEngine.getInstance().sharedPrefUtils.getPref("destination", this));
        }
        setMarkers();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("OnLocationChanged", "yes");
        String currentPosition = this.getResources().getString(R.string.share_location_cur_loc_text);

        if (gmapView.getGoogleMap() != null && location!=null) {
            LatLng gps = new LatLng(location.getLatitude(), location.getLongitude());

            if(dragCamera) {
                marker = gmapView.getGoogleMap().addMarker(new MarkerOptions()
                        .position(gps)
                        .title(currentPosition)
                        .flat(true)
                        .anchor(0.5f,0.5f)
                        .rotation(AppEngine.getInstance().constants.ROTATION)
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.car)));

                if(lineOptions!=null) polyline = gmapView.getGoogleMap().addPolyline(lineOptions);

                gmapView.getGoogleMap().animateCamera(CameraUpdateFactory.newLatLngZoom(gps, 12));
                dragCamera=false;
            } else {
                GmapView.animateMarker(location, marker);
            }

            this.location = location;
            markerRotation = marker.getRotation();
        }
        updateDriection();
    }


    private void setMarkers() {
        LatLng gps = null;

        if(tripState>0 && lineOptions!=null) {
            Log.d("SetMarkers", "Yes");
            if(polyline!=null) polyline.remove();
            polyline = gmapView.getGoogleMap().addPolyline(lineOptions);
            if(tripState>0 && tripState<4) {
                if(pickUpMarker!=null) pickUpMarker.remove();
                gps = new LatLng(tripPickup[0], tripPickup[1]);
                pickUpMarker = gmapView.getGoogleMap().addMarker(new MarkerOptions()
                        .position(gps)
                        .title("Pickup")
                        .icon(BitmapDescriptorFactory.defaultMarker(184)));
            }
            if(destinationMarker!=null) destinationMarker.remove();
            gps = new LatLng(tripDest[0], tripDest[1]);
            destinationMarker = gmapView.getGoogleMap().addMarker(new MarkerOptions()
                    .position(gps)
                    .title("Destination")
                    .icon(BitmapDescriptorFactory.defaultMarker(0)));
        }
    }

    @Override
    public void onMapLoaded() {
        setMarkers();
    }
    
    public void updateDriection(){
        if(tripState > 0 && !AppEngine.getInstance().sharedPrefUtils.getPref("pickUp", this).equals("")){
            String[] pick = AppEngine.getInstance().sharedPrefUtils.getPref("pickUp", this).replace("[", "").replace("]", "").split(",");
            String[] dest = AppEngine.getInstance().sharedPrefUtils.getPref("destination", this).replace("[", "").replace("]", "").split(",");
            if(tripState>3 && location != null) {
                pick = new String[]{String.valueOf(location.getLongitude()), String.valueOf(location.getLatitude())};
            }
            Log.d("pickdest", pick[1]+","+pick[0]+" - "+dest[1]+","+dest[0]);
            dashboardPresenter.getTripDirection(pick[1]+","+pick[0], dest[1]+","+dest[0]);
        }

    }
}

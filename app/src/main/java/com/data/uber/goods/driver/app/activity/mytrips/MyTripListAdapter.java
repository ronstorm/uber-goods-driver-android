package com.data.uber.goods.driver.app.activity.mytrips;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.model.mytrips.TripItem;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.annotations.NonNull;

class ViewHolder {
    TextView txtName;
    ImageView userImage;
    TextView txtDate;
    TextView pickUp;
    TextView destination;
    TextView tripId;
    TextView goodType;
    TextView amount;
    TextView distanceTime;
}

public class MyTripListAdapter extends BaseAdapter{

    private static List<TripItem> myTripList;
    private LayoutInflater mInflater;
    private Context context;

    public MyTripListAdapter(Context context, List<TripItem> values) {
        this.myTripList = values;
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return myTripList.size();
    }

    @Override
    public Object getItem(int position) {
        return myTripList.get(position);
    }

    @NonNull
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.my_trip_listview, parent, false);
            holder = new ViewHolder();
            holder.txtName = convertView.findViewById(R.id.txtName);
            holder.userImage = convertView.findViewById(R.id.userImage);
            holder.txtDate  = convertView.findViewById(R.id.txtDate);
            holder.pickUp = convertView.findViewById(R.id.pickUp);
            holder.destination = convertView.findViewById(R.id.destination);
            holder.tripId = convertView.findViewById(R.id.tripId);
            holder.goodType = convertView.findViewById(R.id.goodType);
            holder.amount = convertView.findViewById(R.id.amount);
            holder.distanceTime = convertView.findViewById(R.id.distanceTime);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }

        if(myTripList.get(position).getDriverProfile()!=null) {
            holder.txtName.setText(myTripList.get(position).getDriverProfile().getUser().getName());
            Picasso.with(context).load(myTripList.get(position).getDriverProfile().getUser().getPicture()).into(holder.userImage);
        }
        //holder.txtDate.setText(myTripList.get(position).getName());
        holder.pickUp.setText(myTripList.get(position).getPickupAddress());
        holder.destination.setText(myTripList.get(position).getDestinationAddress());
        holder.tripId.setText(String.valueOf("Trip #"+myTripList.get(position).getId()));
        holder.destination.setText(myTripList.get(position).getDestinationAddress());
        holder.goodType.setText(myTripList.get(position).getGoodSubCategoryStr());
        holder.amount.setText("SAR "+myTripList.get(position).getAmount());
        holder.distanceTime.setText(myTripList.get(position).getDistanceTravelled()+"KM/"+myTripList.get(position).getTravelDuration()+"m");

        return convertView;
    }
}

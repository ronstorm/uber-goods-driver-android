package com.data.uber.goods.driver.app.activity.authentication;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.R;

/**
 * Created by bitto on 14-Mar-18.
 */

public class ForgetPasswordConfirmFragment extends Fragment implements View.OnClickListener {

    Context context;
    Button confirmCode;
    EditText forgetCode;
    ForgetPasswordMainActivity forgetPasswordMainActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forget_pass_confirm, container, false);
        this.init(view);
        return view;
    }

    public void init(View view) {
        confirmCode = view.findViewById(R.id.confirmCode);
        forgetCode = view.findViewById(R.id.forgetCode);
        confirmCode.setOnClickListener(this);
    }

    public ForgetPasswordConfirmFragment setContext(Context context, ForgetPasswordMainActivity forgetPasswordMainActivity) {
        this.context = context;
        this.forgetPasswordMainActivity = forgetPasswordMainActivity;
        return this;
    }

    @Override
    public void onClick(View view) {
        if(view.equals(confirmCode) && !forgetCode.getText().toString().equals("")) {
            AppEngine.getInstance().constants.forgetCode = Integer.parseInt(forgetCode.getText().toString());
            this.forgetPasswordMainActivity.selectFrag(2);
        }
    }
}

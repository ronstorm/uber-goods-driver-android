package com.data.uber.goods.driver.app.activity.report;

import android.Manifest;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.LinearLayout;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.activity.BaseUIController;
import com.data.uber.goods.driver.app.model.report.IssueCategory;
import com.data.uber.goods.driver.app.presenter.report.ReportIssuePresenter;
import com.data.uber.goods.driver.app.util.GmapView;
import com.data.uber.goods.driver.app.view.report.ReportIssueActivityInterface;
import com.google.android.gms.maps.MapView;
import java.util.List;
import butterknife.ButterKnife;


public class ReportIssueActivity extends BaseUIController implements ReportIssueActivityInterface {

    private GmapView gmapView;
    private ReportIssuePresenter reportIssuePresenter;
    private ReportIssueMainItemView reportIssueMainItemView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.TOPBAR_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.activity_report_problem_main);
        this.init(savedInstanceState);
    }

    private void init(Bundle savedInstanceState) {

        this.setNavBarTitle("");

        reportIssueMainItemView = new ReportIssueMainItemView();

        ButterKnife.inject(this);
        gmapView = new GmapView();

        gmapView.setMapCameraOffset(false);
        gmapView.setLoadCustomMarker(false);
        gmapView.initView(this, (MapView) findViewById(R.id.mapView), savedInstanceState, true);
        gmapView.getMapView().onStart();
        AppEngine.getInstance().googleMapView.setGoogleMapLocationListner(this.gmapView);
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        reportIssuePresenter = new ReportIssuePresenter(this, this);
        reportIssuePresenter.getIssueListAndCategory();
        showloading();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        AppEngine.getInstance().googleMapView.requestPermissionCallback(requestCode, grantResults, this);
    }

    @Override
    public void onIssueListAndCategoryFetched(List<IssueCategory> issueCategories) {
        reportIssueMainItemView.setView(this, this, (LinearLayout) findViewById(R.id.reportListWrapper), issueCategories);
        hideloading();
    }

    @Override
    public void onIssueListAndCategoryFetchError() {
        hideloading();
    }
}

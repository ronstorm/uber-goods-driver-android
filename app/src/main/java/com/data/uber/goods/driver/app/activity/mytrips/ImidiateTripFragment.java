package com.data.uber.goods.driver.app.activity.mytrips;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.model.mytrips.TripItem;
import com.data.uber.goods.driver.app.presenter.mytrip.MytripsPresenter;
import com.data.uber.goods.driver.app.view.mytrip.MyTripsInterface;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by kud-wtag on 6/24/18.
 */

public class ImidiateTripFragment extends Fragment implements MyTripsInterface {

    ListView myTripListView;
    List<TripItem> myTripList;
    MyTripListAdapter myTripListAdapter;
    MytripsPresenter mytripsPresenter;
    Context context;

    public void initialize(Context context) {
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_my_trip, container, false);
        init(savedInstanceState, view);
        return view;
    }

    private void init(Bundle savedInstanceState, View view) {
        myTripListView = (ListView) view.findViewById(R.id.tripsListViewer);
        myTripList = new ArrayList<>();
        myTripListAdapter = new MyTripListAdapter(context, myTripList);
        myTripListView.setAdapter(myTripListAdapter);

        mytripsPresenter = new MytripsPresenter(context, this);
        mytripsPresenter.getAllTrips();
    }

    @Override
    public void tripsFetchSuccess(List<TripItem> tripItemList) {
        this.myTripList.clear();
        this.myTripList.addAll(tripItemList);
        myTripListAdapter.notifyDataSetChanged();
    }

    @Override
    public void tripsFetchError() {

    }
}

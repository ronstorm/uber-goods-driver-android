package com.data.uber.goods.driver.app.view.dashboard;

import com.data.uber.goods.driver.app.model.authentication.CountryEntity;
import com.data.uber.goods.driver.app.model.authentication.UserInfo;
import com.data.uber.goods.driver.app.model.mytrips.CustomerProfile;

import java.util.HashMap;
import java.util.List;


public interface DashboardInterface {
    void setUserDetails(UserInfo userInfo);
    void acceptRequest();
    void onTheWay();
    void startTrip();
    void endTrip();
    void rejectTripAnyState();
    void setDirection(List<List<HashMap<String, String>>> routes);
    void setRating();
    void getCustomerProfile(CustomerProfile customerProfile);
}

package com.data.uber.goods.driver.app.util;

import android.location.Location;

/**
 * Created by kud-wtag on 3/4/18.
 */

public interface GoogleMapLocationListner {
    public void onLocationChanged(Location location);
}

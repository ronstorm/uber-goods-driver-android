package com.data.uber.goods.driver.app.presenter.authentication;

import android.content.Context;
import android.widget.Toast;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.model.GeoLocationResponse;
import com.data.uber.goods.driver.app.model.authentication.LoginCredentials;
import com.data.uber.goods.driver.app.model.authentication.LoginResponse;
import com.data.uber.goods.driver.app.model.authentication.UserInfo;
import com.data.uber.goods.driver.app.service.network.LoginService;
import com.data.uber.goods.driver.app.service.network.RegistrationService;
import com.data.uber.goods.driver.app.view.authentication.LoginIterface;
import com.data.uber.goods.driver.app.view.authentication.RegistrationInterface;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class RegistrationPresenter {
    private Context context;
    private RegistrationInterface registrationInterface;

    public RegistrationPresenter(Context context, RegistrationInterface registrationInterface) {
        this.context = context;
        this.registrationInterface = registrationInterface;
    }

    public void getDefaultLocation(String latLng) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.create(RegistrationService.class, AppEngine.getInstance().constants.GOOGLE_MAP_GEO_CODING_BASE_URL)
                .getAddressFromLatLng(latLng, AppEngine.getInstance().constants.GOOGLE_API_KEY), new Observer<GeoLocationResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }
            @Override
            public void onError(@NonNull Throwable e) {
                System.out.println(e.toString());
            }
            @Override
            public void onComplete() {

            }
            @Override
            public void onNext(@NonNull GeoLocationResponse geoLocationResponse) {
                System.out.println(geoLocationResponse.getResults().size());
                if(geoLocationResponse.getResults().size()>0) {
                    registrationInterface.setDefaultLocation(geoLocationResponse.getResults().get(0).getFormatted_address());
                }
            }
        });
    }

    public void registerUser(final UserInfo userInfo){
        System.out.println(userInfo);
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(RegistrationService.class, context).registerUser(userInfo), new Observer<UserInfo>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }
            @Override
            public void onError(@NonNull Throwable e) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
            }
            @Override
            public void onComplete() {

            }
            @Override
            public void onNext(@NonNull UserInfo userInfo1) {
                Toast.makeText(context, "Registration Successful!!", Toast.LENGTH_LONG).show();
            }
        });
    }

}

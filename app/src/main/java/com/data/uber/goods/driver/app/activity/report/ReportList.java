package com.data.uber.goods.driver.app.activity.report;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bitto on 10-Apr-18.
 */

public class ReportList {
    public List<ReportItem> getList() {
        List<ReportItem> reportItems = new ArrayList<>();
        List<ReportItem> subReportItems = new ArrayList<>();

        ReportItem reportItem = new ReportItem();
        ReportItem subReportItem = new ReportItem();
        reportItem.setItem("On a Trip");

        subReportItem.setItem("Cant Change My Number");
        subReportItems.add(subReportItem);

        subReportItem = new ReportItem();
        subReportItem.setItem("Cant Change My Time");
        subReportItems.add(subReportItem);

        subReportItem = new ReportItem();
        subReportItem.setItem("Cant Change My Name");
        subReportItems.add(subReportItem);

        subReportItem = new ReportItem();
        subReportItem.setItem("Cant Change My car info");
        subReportItems.add(subReportItem);

        reportItem.setReportItems(subReportItems);

        reportItems.add(reportItem);

        reportItem = new ReportItem();
        reportItem.setItem("Payment");
        reportItem.setReportItems(subReportItems);

        reportItems.add(reportItem);

        reportItem = new ReportItem();
        reportItem.setItem("Account");

        subReportItem = new ReportItem();
        subReportItem.setItem("Account problem");
        subReportItems.add(subReportItem);
        reportItem.setReportItems(subReportItems);

        reportItems.add(reportItem);

        return reportItems;
    }
}

package com.data.uber.goods.driver.app.service.network;


import com.data.uber.goods.driver.app.model.report.IssueCategory;
import com.data.uber.goods.driver.app.model.report.IssueCreateRequest;
import com.data.uber.goods.driver.app.model.report.IssueCreateResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface IssueApiService {
    @GET("v1/issue/categories/")
    Observable<List<IssueCategory>> getIssueCategoryAndTopics();

    @POST("v1/issue/")
    Observable<IssueCreateResponse> submitIssue(@Body IssueCreateRequest issueCreateRequest);
}

package com.data.uber.goods.driver.app;

import com.data.uber.goods.driver.app.util.AndroidIntent;
import com.data.uber.goods.driver.app.util.Animation;
import com.data.uber.goods.driver.app.util.Base64Converter;
import com.data.uber.goods.driver.app.util.BlurEditText;
import com.data.uber.goods.driver.app.util.CommonUtils;
import com.data.uber.goods.driver.app.util.Constants;
import com.data.uber.goods.driver.app.util.DateTimeHelper;
import com.data.uber.goods.driver.app.util.GoogleMapView;
import com.data.uber.goods.driver.app.util.ListViewHelper;
import com.data.uber.goods.driver.app.util.MenuItemList;
import com.data.uber.goods.driver.app.util.NetworkAdapter;
import com.data.uber.goods.driver.app.util.NetworkUtils;
import com.data.uber.goods.driver.app.util.SVGImage.SVGImageRederer;
import com.data.uber.goods.driver.app.util.SharedPrefUtils;
import com.data.uber.goods.driver.app.util.SpinnerUtil;
import com.data.uber.goods.driver.app.util.StorageImageHelper;
import com.data.uber.goods.driver.app.util.TimerUtil;
import com.data.uber.goods.driver.app.util.socket.io.SocketIO;

public class AppEngine {
    private static volatile AppEngine ourInstance;
    private static final Object mutex = new Object();

    public final NetworkAdapter networkAdapter;
    public final Constants constants;
    public final AndroidIntent androidIntent;
    public final BlurEditText blurEditText;
    public final GoogleMapView googleMapView;
    private final SpinnerUtil spinnerUtil;
    public final MenuItemList menuItemList;
    private final ListViewHelper listViewHelper;
    public final SharedPrefUtils sharedPrefUtils;
    private final CommonUtils commonUtils;
    private final SocketIO socketIO;
    private final Base64Converter base64Converter;
    private final DateTimeHelper dateTimeHelper;
    public final NetworkUtils networkUtils;
    public final Animation animation;
    public final TimerUtil timerUtil;
    public final StorageImageHelper storageImageHelper;
    public final SVGImageRederer svgImageRederer;

    private AppEngine() {
        networkAdapter = new NetworkAdapter();
        constants = new Constants();
        timerUtil = new TimerUtil();
        androidIntent = new AndroidIntent();
        blurEditText = new BlurEditText();
        googleMapView = new GoogleMapView();
        svgImageRederer = new SVGImageRederer();
        spinnerUtil = new SpinnerUtil();
        menuItemList = new MenuItemList();
        listViewHelper = new ListViewHelper();
        sharedPrefUtils =  new SharedPrefUtils();
        commonUtils = new CommonUtils();
        socketIO = new SocketIO();
        animation = new Animation();
        base64Converter = new Base64Converter();
        dateTimeHelper = new DateTimeHelper();
        networkUtils = new NetworkUtils();
        storageImageHelper = new StorageImageHelper();
    }

    public static AppEngine getInstance() {
        AppEngine result = ourInstance;
        if (result == null) {
            synchronized (mutex) {
                result = ourInstance;
                if (result == null)
                    ourInstance = result = new AppEngine();
            }
        }
        return result;
    }
}

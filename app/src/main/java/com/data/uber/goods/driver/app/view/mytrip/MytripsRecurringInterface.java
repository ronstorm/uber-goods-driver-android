package com.data.uber.goods.driver.app.view.mytrip;


import com.data.uber.goods.driver.app.model.mytrips.TripItem;

import java.util.List;

/**
 * Created by kud-wtag on 6/24/18.
 */

public interface MytripsRecurringInterface {
    void tripsFetchSuccess(List<TripItem> tripItemList);
    void tripsFetchError();
}

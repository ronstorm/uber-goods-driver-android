package com.data.uber.goods.driver.app.activity.settings;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;


import com.data.uber.goods.driver.app.R;

import java.util.List;


public class SettingsLanguageListAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    List<String> languageList;

    public SettingsLanguageListAdapter(Context context, List<String> values) {
        this.languageList = values;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return languageList.size();
    }

    @Override
    public Object getItem(int position) {
        return languageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.settings_screen_language_list_item, parent, false);
            holder = new ViewHolder();
            holder.language = convertView.findViewById(R.id.languageButton);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.language.setText(languageList.get(position));
        return convertView;
    }

}

class ViewHolder {
    RadioButton language;
}

package com.data.uber.goods.driver.app.model.authentication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserLoginDetail {

    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("birth_date")
    @Expose
    private String birthDate;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("driving_license_number")
    @Expose
    private String drivingLicenseNumber;
    @SerializedName("driving_license_renewal_date")
    @Expose
    private String drivingLicenseRenewalDate;
    @SerializedName("passport_number")
    @Expose
    private String passportNumber;
    @SerializedName("passport_expiry_date")
    @Expose
    private String passportExpiryDate;
    @SerializedName("work_permit_number")
    @Expose
    private String workPermitNumber;
    @SerializedName("area_of_interest")
    @Expose
    private String areaOfInterest;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("edited_at")
    @Expose
    private String editedAt;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getDrivingLicenseNumber() {
        return drivingLicenseNumber;
    }

    public void setDrivingLicenseNumber(String drivingLicenseNumber) {
        this.drivingLicenseNumber = drivingLicenseNumber;
    }

    public String getDrivingLicenseRenewalDate() {
        return drivingLicenseRenewalDate;
    }

    public void setDrivingLicenseRenewalDate(String drivingLicenseRenewalDate) {
        this.drivingLicenseRenewalDate = drivingLicenseRenewalDate;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPassportExpiryDate() {
        return passportExpiryDate;
    }

    public void setPassportExpiryDate(String passportExpiryDate) {
        this.passportExpiryDate = passportExpiryDate;
    }

    public String getWorkPermitNumber() {
        return workPermitNumber;
    }

    public void setWorkPermitNumber(String workPermitNumber) {
        this.workPermitNumber = workPermitNumber;
    }

    public String getAreaOfInterest() {
        return areaOfInterest;
    }

    public void setAreaOfInterest(String areaOfInterest) {
        this.areaOfInterest = areaOfInterest;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getEditedAt() {
        return editedAt;
    }

    public void setEditedAt(String editedAt) {
        this.editedAt = editedAt;
    }

}

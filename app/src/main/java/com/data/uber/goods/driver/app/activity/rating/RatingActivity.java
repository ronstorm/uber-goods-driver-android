package com.data.uber.goods.driver.app.activity.rating;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RatingBar;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.activity.BaseUIController;
import com.data.uber.goods.driver.app.model.rating.Rating;
import com.data.uber.goods.driver.app.presenter.rating.RatingPresenter;
import com.data.uber.goods.driver.app.view.rating.RatingViewInterface;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class RatingActivity extends BaseUIController implements RatingViewInterface{

    @InjectView(R.id.ratingSubmitBtn)
    Button ratingSubmitBtn;

    @InjectView(R.id.ratingBar)
    RatingBar ratingBar;

    private RatingPresenter ratingPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setLayoutType(AppEngine.getInstance().constants.DRAWER_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.activity_rating);
        this.init();
        ButterKnife.inject(this);
    }

    private void init() {
        this.ratingPresenter = new RatingPresenter(this, this);
    }

    @OnClick(R.id.ratingSubmitBtn)
    public void submitRating(){
        Rating rating = new Rating();
        rating.setTrip(42);
        rating.setRating((int)ratingBar.getRating());
        rating.setComment("Test Comment");
        this.ratingPresenter.submitRating(rating);
    }

}

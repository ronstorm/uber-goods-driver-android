package com.data.uber.goods.driver.app.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.activity.authentication.LoginActivity;
import com.data.uber.goods.driver.app.activity.mytrips.MyTripActivity;
import com.data.uber.goods.driver.app.activity.rating.RatingActivity;
import com.data.uber.goods.driver.app.activity.report.ReportIssueActivity;
import com.data.uber.goods.driver.app.activity.settings.SettingsActivity;
import com.data.uber.goods.driver.app.util.firebase.FireBaseInstanceIdService;

import java.util.ArrayList;


public class MenuItemList {
    public ArrayList<MenuItem> getMenuList(final Context context, final Activity activity) {
        ArrayList<MenuItem> menuItemList = new ArrayList<MenuItem>();

        MenuItem menuItem = new MenuItem();
        menuItem.setTitle("My trips");
        menuItem.setActivityClass(MyTripActivity.class);
        menuItem.setImageResId(R.mipmap.my_tips);
        menuItem.setLayoutType(1);
        menuItemList.add(menuItem);

        menuItem = new MenuItem();
        menuItem.setTitle("Settings");
        menuItem.setActivityClass(SettingsActivity.class);
        menuItem.setImageResId(R.mipmap.settings);
        menuItem.setLayoutType(1);
        menuItemList.add(menuItem);

//        menuItem = new MenuItem();
//        menuItem.setTitle("Payment");
//        //menuItem.setActivityClass(activity.class);
//        menuItem.setImageResId(R.mipmap.payment);
//        menuItem.setLayoutType(2);
//        menuItem.setMenuItemClickListner(new MenuItemClickListner() {
//            @Override
//            public void onMenuItemClick() {
//                Toast.makeText(activity, "Under construction :)" , Toast.LENGTH_LONG).show();
//            }
//        });
//        menuItemList.add(menuItem);

        menuItem = new MenuItem();
        menuItem.setTitle("Report an Issue");
        menuItem.setActivityClass(ReportIssueActivity.class);
        menuItem.setImageResId(R.mipmap.report_issue);
        menuItem.setLayoutType(1);
        menuItemList.add(menuItem);

        menuItem = new MenuItem();
        menuItem.setTitle("Logout");
        //menuItem.setActivityClass(activity.class);
        menuItem.setImageResId(R.mipmap.logout);
        menuItem.setLayoutType(2);
        menuItem.setMenuItemClickListner(new MenuItemClickListner() {
            @Override
            public void onMenuItemClick() {
                FireBaseInstanceIdService fireBaseInstanceIdService = new FireBaseInstanceIdService();
                fireBaseInstanceIdService.deleteFcmToken();
                AppEngine.getInstance().sharedPrefUtils.putPref(AppEngine.getInstance().constants.OAUTH_ACCESS_TOKEN, "", context);
                AppEngine.getInstance().sharedPrefUtils.putPref(AppEngine.getInstance().constants.OAUTH_TOKEN_TYPE, "", context);
                AppEngine.getInstance().sharedPrefUtils.putPref(AppEngine.getInstance().constants.OAUTH_REFRESH_TOKEN, "", context);
                AppEngine.getInstance().sharedPrefUtils.clearPref("tripId", context);
                AppEngine.getInstance().sharedPrefUtils.clearPref("tripState", context);
                activity.finishAffinity();
            }
        });
        menuItemList.add(menuItem);

        menuItem = new MenuItem();
        menuItem.setLayoutType(3);
        menuItemList.add(menuItem);

        menuItem = new MenuItem();
        menuItem.setTitle("Terms & Conditions");
        //menuItem.setActivityClass(activity.class);
        menuItem.setImageResId(R.mipmap.logout);
        menuItem.setLayoutType(2);
        menuItem.setMenuItemClickListner(new MenuItemClickListner() {
            @Override
            public void onMenuItemClick() {
                Toast.makeText(context, "TC", Toast.LENGTH_SHORT).show();
            }
        });
        menuItemList.add(menuItem);

        menuItem = new MenuItem();
        menuItem.setTitle("Rate This App");
        menuItem.setActivityClass(RatingActivity.class);
        menuItem.setImageResId(R.mipmap.logout);
        menuItem.setLayoutType(2);
//        menuItem.setMenuItemClickListner(new MenuItemClickListner() {
//            @Override
//            public void onMenuItemClick() {
//                Toast.makeText(context, "RTA", Toast.LENGTH_SHORT).show();
//            }
//        });
        menuItemList.add(menuItem);

        menuItem = new MenuItem();
        menuItem.setTitle("About");
        //menuItem.setActivityClass(activity.class);
        menuItem.setImageResId(R.mipmap.logout);
        menuItem.setLayoutType(2);
        menuItem.setMenuItemClickListner(new MenuItemClickListner() {
            @Override
            public void onMenuItemClick() {
                Toast.makeText(activity, "Under construction :)" , Toast.LENGTH_LONG).show();
            }
        });
        menuItemList.add(menuItem);

        return menuItemList;
    }
}

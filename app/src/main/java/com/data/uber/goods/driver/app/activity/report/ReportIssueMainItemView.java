package com.data.uber.goods.driver.app.activity.report;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.model.report.IssueCategory;

import java.util.ArrayList;
import java.util.List;


public class ReportIssueMainItemView {

    public void setView(final Activity activity, final Context context, LinearLayout menuList, final List<IssueCategory> issueCategories) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final List<MenuItemViewHolder> menuItemViewHolders = new ArrayList<>();
        for (int i = 0; i<issueCategories.size(); i++) {
            View view  = null;
            view = inflater.inflate(R.layout.activity_report_problem_item_list, menuList, false);

            final MenuItemViewHolder viewHolder = new MenuItemViewHolder();

            viewHolder.button = view.findViewById(R.id.reportItem);
            viewHolder.navButtonIcon = view.findViewById(R.id.reportItemArrow);
            viewHolder.subMenu = view.findViewById(R.id.subMenu);
            view.setTag(viewHolder);

            MenuItemViewHolder holder = (MenuItemViewHolder) view.getTag();
            holder.button.setText(issueCategories.get(i).getName());

            final int finalI = i;
            holder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(viewHolder.subMenu.getVisibility()==View.INVISIBLE || viewHolder.subMenu.getVisibility()==View.GONE) {
                        viewHolder.subMenu.setVisibility(View.VISIBLE);
                        Animation rotateLeftUp = AnimationUtils.loadAnimation(context, R.anim.rotate_left_up);
                        viewHolder.navButtonIcon.startAnimation(rotateLeftUp);
                    } else {
                        viewHolder.subMenu.setVisibility(View.GONE);
                        Animation rotateLeftUp = AnimationUtils.loadAnimation(context, R.anim.rotate_right_down);
                        viewHolder.navButtonIcon.startAnimation(rotateLeftUp);
                    }
                    for(int j=0; j<issueCategories.size(); j++) {
                        if(j!=finalI) {
                            if(menuItemViewHolders.get(j).subMenu.getVisibility()==View.VISIBLE) {
                                Animation rotateLeftUp = AnimationUtils.loadAnimation(context, R.anim.rotate_right_down);
                                menuItemViewHolders.get(j).navButtonIcon.startAnimation(rotateLeftUp);
                            }
                            menuItemViewHolders.get(j).subMenu.setVisibility(View.GONE);
                        }
                    }
                }
            });
            menuList.addView(view);

            menuItemViewHolders.add(viewHolder);
            ReportIssueSubItemView reportIssueSubItemView = new ReportIssueSubItemView();
            reportIssueSubItemView.setView(activity, context, viewHolder.subMenu, issueCategories.get(i).getTopics());
        }
    }

}

class MenuItemViewHolder {
    public Button button;
    public ImageView navButtonIcon;
    public LinearLayout subMenu;
}
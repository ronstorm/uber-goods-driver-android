package com.data.uber.goods.driver.app.service.network;

import com.data.uber.goods.driver.app.model.authentication.CountryEntity;
import com.google.gson.JsonArray;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface CountryListRestService {
    @GET("v1/location/")
    Observable<List<CountryEntity>> getAllCountries();
}

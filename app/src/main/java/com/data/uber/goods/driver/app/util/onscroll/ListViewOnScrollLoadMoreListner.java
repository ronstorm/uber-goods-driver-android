package com.data.uber.goods.driver.app.util.onscroll;

public interface ListViewOnScrollLoadMoreListner {
    void onScrollLoadMore();
}

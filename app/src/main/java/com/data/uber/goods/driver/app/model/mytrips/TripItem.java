package com.data.uber.goods.driver.app.model.mytrips;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kud-wtag on 6/24/18.
 */

public class TripItem {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("customer")
    @Expose
    private String customer;
    @SerializedName("driver")
    @Expose
    private String driver;
    @SerializedName("pickup_address")
    @Expose
    private String pickupAddress;
    @SerializedName("destination_address")
    @Expose
    private String destinationAddress;
    @SerializedName("good_category")
    @Expose
    private String goodCategory;
    @SerializedName("good_sub_category")
    @Expose
    private String goodSubCategory;
    @SerializedName("product_size")
    @Expose
    private String productSize;
    @SerializedName("product_weight")
    @Expose
    private String productWeight;
    @SerializedName("worker_quantity")
    @Expose
    private String workerQuantity;
    @SerializedName("delivery_type")
    @Expose
    private String deliveryType;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("distance_travelled")
    @Expose
    private String distanceTravelled;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("travel_duration")
    @Expose
    private String travelDuration;
    @SerializedName("good_sub_category_str")
    @Expose
    private String goodSubCategoryStr;

    private DriverProfile driverProfile;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(String pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getGoodCategory() {
        return goodCategory;
    }

    public void setGoodCategory(String goodCategory) {
        this.goodCategory = goodCategory;
    }

    public String getGoodSubCategory() {
        return goodSubCategory;
    }

    public void setGoodSubCategory(String goodSubCategory) {
        this.goodSubCategory = goodSubCategory;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }

    public String getProductWeight() {
        return productWeight;
    }

    public void setProductWeight(String productWeight) {
        this.productWeight = productWeight;
    }

    public String getWorkerQuantity() {
        return workerQuantity;
    }

    public void setWorkerQuantity(String workerQuantity) {
        this.workerQuantity = workerQuantity;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDistanceTravelled() {
        return distanceTravelled;
    }

    public void setDistanceTravelled(String distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTravelDuration() {
        return travelDuration;
    }

    public void setTravelDuration(String travelDuration) {
        this.travelDuration = travelDuration;
    }

    public String getGoodSubCategoryStr() {
        return goodSubCategoryStr;
    }

    public void setGoodSubCategoryStr(String goodSubCategoryStr) {
        this.goodSubCategoryStr = goodSubCategoryStr;
    }

    public DriverProfile getDriverProfile() {
        return driverProfile;
    }

    public void setDriverProfile(DriverProfile driverProfile) {
        this.driverProfile = driverProfile;
    }
}

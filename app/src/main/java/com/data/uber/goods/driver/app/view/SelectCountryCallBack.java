package com.data.uber.goods.driver.app.view;

import com.data.uber.goods.driver.app.model.authentication.CountryEntity;

public interface SelectCountryCallBack {
    void CountryCallBack(CountryEntity cEntity);
}

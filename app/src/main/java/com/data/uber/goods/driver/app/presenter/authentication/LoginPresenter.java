package com.data.uber.goods.driver.app.presenter.authentication;

import android.content.Context;
import android.widget.Toast;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.model.authentication.LoginCredentials;
import com.data.uber.goods.driver.app.model.authentication.LoginResponse;
import com.data.uber.goods.driver.app.model.authentication.UserInfo;
import com.data.uber.goods.driver.app.service.network.DashboardRestService;
import com.data.uber.goods.driver.app.service.network.LoginService;
import com.data.uber.goods.driver.app.view.authentication.LoginIterface;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class LoginPresenter {
    private final Context context;
    private final LoginIterface loginIterface;

    public LoginPresenter(Context context, LoginIterface loginIterface) {
        this.context = context;
        this.loginIterface = loginIterface;
    }

    public void getDriverProfile() {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(DashboardRestService.class, context).
                        getDriverProfileInfo(),
                new Observer<UserInfo>() {
                    @Override
                    public void onSubscribe(@android.support.annotation.NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@android.support.annotation.NonNull Throwable e) {
                        Toast.makeText(context, "Login " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(@android.support.annotation.NonNull UserInfo userInfo) {
                        AppEngine.getInstance().sharedPrefUtils.putPref("driverId", String.valueOf(userInfo.getId()), context);
                        loginIterface.userProfile(userInfo);
                    }
                });
    }

    public void driverLogin(String username, String password) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.create(LoginService.class)
                .login(username, password, "password"), new Observer<LoginResponse>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(LoginResponse loginResponse) {
                AppEngine.getInstance().sharedPrefUtils.putPref(AppEngine.getInstance().constants.OAUTH_ACCESS_TOKEN, loginResponse.getAccessToken(), context);
                AppEngine.getInstance().sharedPrefUtils.putPref(AppEngine.getInstance().constants.OAUTH_TOKEN_TYPE, loginResponse.getTokenType(), context);
                AppEngine.getInstance().sharedPrefUtils.putPref(AppEngine.getInstance().constants.OAUTH_REFRESH_TOKEN, loginResponse.getRefreshToken(), context);
                if(!loginResponse.getAccessToken().isEmpty()){
                    loginIterface.loginAPI();
                }
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(context, "wrong username or password", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onComplete() {

            }
        });
    }
}

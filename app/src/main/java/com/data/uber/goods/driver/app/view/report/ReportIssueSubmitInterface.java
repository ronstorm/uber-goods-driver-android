package com.data.uber.goods.driver.app.view.report;


import com.data.uber.goods.driver.app.model.report.IssueCreateResponse;

public interface ReportIssueSubmitInterface {
    void onSubmitApiSuccess(IssueCreateResponse issueCreateResponse);
    void onSubmitApiError();
}

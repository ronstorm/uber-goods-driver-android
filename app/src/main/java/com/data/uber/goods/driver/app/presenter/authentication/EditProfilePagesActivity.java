package com.data.uber.goods.driver.app.presenter.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.activity.BaseUIController;
import com.data.uber.goods.driver.app.model.authentication.CountryEntity;
import com.data.uber.goods.driver.app.view.CloseFragment;
import com.data.uber.goods.driver.app.view.CountrySelect;
import com.data.uber.goods.driver.app.view.SelectCountryCallBack;
import com.data.uber.goods.driver.app.view.SelectCountryInterface;

import java.util.ArrayList;
import java.util.List;


public class EditProfilePagesActivity extends BaseUIController implements CountrySelect,SelectCountryInterface,CloseFragment,View.OnClickListener {

    private EditProfileViewPagerAdapter editProfileViewPagerAdapter;
    boolean initActivity;
    public static List<CountryEntity> countryEntity;
    private SelectCountryCallBack selectCountryCallBack;
    private ListView countryListView;
    private SelectCountry countrySelectPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.countryEntity = new ArrayList<CountryEntity>();
        this.initActivity = true;
        this.setLayoutType(AppEngine.getInstance().constants.TOPBAR_LAYOUT);
        super.onCreate(savedInstanceState);
        this.addLayout(R.layout.create_account_country_list_activity);
        this.countryListView = findViewById(R.id.countryListView);
        countrySelectPresenter = new SelectCountry(this, this);
        countrySelectPresenter.loadCountries(this);

    }

    @Override
    public void selectCountry(int id, String countryName) {
        Intent intent = new Intent();
        intent.putExtra("countryId", String.valueOf(id));
        intent.putExtra("countryName", countryName);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        this.onBackPressed();
    }

    @Override
    public void onCloseFragment() {

    }

    @Override
    public void setCountryListOnView(List<CountryEntity> countryEntity) {
        this.countryEntity = countryEntity;
        EditProfileAddressSelectCoutryListAdapter adapter = new EditProfileAddressSelectCoutryListAdapter(this, this.countryEntity, this, this);
        countryListView.setAdapter(adapter);
    }
}

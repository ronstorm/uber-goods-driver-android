package com.data.uber.goods.driver.app.presenter.authentication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.model.authentication.CountryEntity;
import com.data.uber.goods.driver.app.view.CloseFragment;
import com.data.uber.goods.driver.app.view.CountrySelect;

import java.util.List;


public class EditProfileAddressSelectCoutryListAdapter extends ArrayAdapter<CountryEntity> {
    private final Context context;
    private List<CountryEntity> countryEntity = null;
    private CountryListViewHolder viewHolder;
    private CountrySelect countrySelect;
    private CloseFragment closeFragment;

    public EditProfileAddressSelectCoutryListAdapter(Context context, List<CountryEntity> countryEntity, CountrySelect countrySelect, CloseFragment closeFragment) {
        super(context, -1, countryEntity);
        this.context = context;
        this.countryEntity = countryEntity;
        this.countrySelect = countrySelect;
        this.closeFragment = closeFragment;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        View rowView = convertView;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.create_account_country_list, parent, false);
            CountryListViewHolder viewHolder = new CountryListViewHolder();
            viewHolder.countryName = rowView.findViewById(R.id.countryName);
            viewHolder.countryImage = rowView.findViewById(R.id.countryImage);
            viewHolder.countryContainer = rowView.findViewById(R.id.countryContainer);
            rowView.setTag(viewHolder);
        }

        CountryListViewHolder holder = (CountryListViewHolder) rowView.getTag();
        holder.countryName.setText(countryEntity.get(position).getName());

        holder.countryContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countrySelect.selectCountry(countryEntity.get(position).getId(), countryEntity.get(position).getName().toString());
                closeFragment.onCloseFragment();
            }
        });

        return rowView;
    }
}

class CountryListViewHolder {
    public TextView countryName;
    public ImageView countryImage;
    public LinearLayout countryContainer;
}

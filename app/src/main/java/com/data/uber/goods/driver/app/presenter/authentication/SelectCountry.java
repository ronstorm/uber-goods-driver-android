package com.data.uber.goods.driver.app.presenter.authentication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.model.authentication.CountryEntity;
import com.data.uber.goods.driver.app.service.network.CountryListRestService;
import com.data.uber.goods.driver.app.view.SelectCountryInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import java.util.ArrayList;
import java.util.List;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


public class SelectCountry {
    private final Context context;
    private SelectCountryInterface selectCountryInterface;

    public SelectCountry(Context context, SelectCountryInterface selectCountryInterface) {

        this.context = context;
        this.selectCountryInterface = selectCountryInterface;
    }

    public void loadCountries(final EditProfilePagesActivity editProfilePagesActivity) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.create(CountryListRestService.class).getAllCountries(),
            new Observer<List<CountryEntity>>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {

                }
                @Override
                public void onError(@NonNull Throwable e) {
                    if(editProfilePagesActivity!=null) editProfilePagesActivity.hideloading();
                }
                @Override
                public void onComplete() {
                    if(editProfilePagesActivity!=null) editProfilePagesActivity.hideloading();
                }
                @Override
                public void onNext(@NonNull List<CountryEntity> countryEntities) {
                    selectCountryInterface.setCountryListOnView(countryEntities);
                    if(editProfilePagesActivity!=null) editProfilePagesActivity.hideloading();
                }
        });
    }
}

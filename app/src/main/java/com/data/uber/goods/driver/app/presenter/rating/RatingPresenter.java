package com.data.uber.goods.driver.app.presenter.rating;


import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.model.authentication.UserInfo;
import com.data.uber.goods.driver.app.model.mytrips.AcceptTripResponse;
import com.data.uber.goods.driver.app.model.rating.Rating;
import com.data.uber.goods.driver.app.service.network.DashboardRestService;
import com.data.uber.goods.driver.app.service.network.RatingRestService;
import com.data.uber.goods.driver.app.view.rating.RatingViewInterface;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class RatingPresenter {
    private Context context;
    private RatingViewInterface ratingViewInterface;

    public RatingPresenter(Context context, RatingViewInterface ratingViewInterface){
        this.context = context;
        this.ratingViewInterface = ratingViewInterface;
    }

    public void submitRating(Rating rating) {
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(RatingRestService.class, context).submitRating(rating),
                new Observer<AcceptTripResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onComplete() {

                    }
                    @Override
                    public void onNext(@NonNull AcceptTripResponse acceptTripResponse) {
                        Toast.makeText(context, "Success", Toast.LENGTH_LONG).show();
                    }
                });
    }
}

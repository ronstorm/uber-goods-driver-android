package com.data.uber.goods.driver.app.util.firebase;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.data.uber.goods.driver.app.AppEngine;
import com.data.uber.goods.driver.app.model.notification.FirebaseRegistration;
import com.data.uber.goods.driver.app.service.network.LoginService;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;


import java.io.IOException;
import java.util.UUID;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;



public class FireBaseInstanceIdService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {

        //For registration of token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        //To displaying token on logcat
        Log.d("TOKEN: ", refreshedToken);

    }

    public void deleteFcmToken() {
        try {
            FirebaseInstanceId.getInstance().deleteToken(FirebaseInstanceId.getInstance().getId(), FirebaseMessaging.INSTANCE_ID_SCOPE);
            FirebaseInstanceId.getInstance().deleteInstanceId();
            FirebaseInstanceId.getInstance().getToken();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void registerFcmToken(final Context context) {
        FirebaseInstanceId.getInstance().getToken();
        FirebaseRegistration firebaseRegistration = new FirebaseRegistration();
        firebaseRegistration.setActive(true);
        firebaseRegistration.setName(android.os.Build.MODEL+" "+android.os.Build.MANUFACTURER + " " + android.os.Build.PRODUCT);
        firebaseRegistration.setType("android");
        firebaseRegistration.setDeviceId(UUID.randomUUID().toString());
        firebaseRegistration.setRegistrationId(FirebaseInstanceId.getInstance().getToken());
        AppEngine.getInstance().networkAdapter.subscriber(AppEngine.getInstance().networkAdapter.callAPI(LoginService.class, context).fcmRegistration(firebaseRegistration),
                new Observer<FirebaseRegistration>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull FirebaseRegistration firebaseRegistration) {
//                        Toast.makeText(context, "FCM Registered", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
//                        Toast.makeText(context, "FCM Error", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}

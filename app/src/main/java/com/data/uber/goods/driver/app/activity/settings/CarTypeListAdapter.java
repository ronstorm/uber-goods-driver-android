package com.data.uber.goods.driver.app.activity.settings;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;


import com.data.uber.goods.driver.app.R;
import com.data.uber.goods.driver.app.model.settings.CarType;

import java.util.List;


public class CarTypeListAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    List<CarType> carTypeList;

    public CarTypeListAdapter(Context context, List<CarType> values) {
        this.carTypeList = values;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return carTypeList.size();
    }

    @Override
    public Object getItem(int position) {
        return carTypeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        CarTypeViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.simple_list_view, parent, false);
            holder = new CarTypeViewHolder();
            holder.carType = convertView.findViewById(R.id.itemText);
            convertView.setTag(holder);
        } else {
            holder = (CarTypeViewHolder) convertView.getTag();
        }
        holder.carType.setText(carTypeList.get(position).getName());
        return convertView;
    }

}

class CarTypeViewHolder {
    TextView carType;
}
